package edu.birzeit.jmse.sbse;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;


public class HrResourcesNSGAII_main {
	
	  public static Logger      logger_ ;      // Logger object
	  public static FileHandler fileHandler_ ; // FileHandler object

	public static void main(String[] args) throws Exception {
		Problem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		HashMap parameters; // Operator parameters
		
		String problemName = "resources/bugs50.bugs";
		String employeeFilePath = "resources/Employee8.emp";
		int bits = 450;

		QualityIndicator indicators; // Object to get quality indicators

		// Logger object and file to store log messages
		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("NSGAII_main.log");
		logger_.addHandler(fileHandler_);

		
		problem = new MoHrResources("Binary", problemName, employeeFilePath);
		
		algorithm = new NSGAII(problem);
		
		indicators = new QualityIndicator(problem,"FUN");
		// algorithm = new ssNSGAII(problem);

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", 1000);
		algorithm.setInputParameter("maxEvaluations", 250000);

		/* Crossver operator */
		parameters = new HashMap();
		parameters.put("probability", 0.80);
		// crossover =
		// Mutation and Crossover for Binary codification 
	    parameters = new HashMap() ;
	    parameters.put("probability", 0.80) ;
	    crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);                   

	    parameters = new HashMap() ;
	    parameters.put("probability", 0.05) ;
	    mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);                    
	    
	    /* Selection Operator */
	    parameters = null ;
	    selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters) ;   

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Add the indicator object to the algorithm
		algorithm.setInputParameter("indicators", indicators);

		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		logger_.info("Total execution time: " + estimatedTime + "ms");
		logger_.info("Variables values have been writen to file VAR");
		population.printVariablesToFile("VAR");
		logger_.info("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");

		if (indicators != null) {
			logger_.info("Quality indicators");
			logger_.info("Hypervolume: " + indicators.getHypervolume(population));
			logger_.info("GD         : " + indicators.getGD(population));
			logger_.info("IGD        : " + indicators.getIGD(population));
			logger_.info("Spread     : " + indicators.getSpread(population));
			logger_.info("Epsilon    : " + indicators.getEpsilon(population));

			int evaluations = ((Integer) algorithm.getOutputParameter("evaluations")).intValue();
			logger_.info("Speed      : " + evaluations + " evaluations");
		} // if
		System.out.println(problem);
	} // main

}
