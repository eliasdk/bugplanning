package edu.birzeit.jmse.sbse.reporting;

import edu.birzeit.jmse.sbse.Constants;
import edu.birzeit.jmse.sbse.FileUtils;

public class RadarChartGenerator {

	public static void main(String[] args) {
		addSolutionChart("/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/StudyOutput/xcisco_reducedBugsSkills_200/data/NSGAII/xcisco_reducedBugsSkills/") ; 
	}
	public static void addSolutionChart(String funFolder ) {
		//String funFolder = "/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/StudyOutput/xcisco_reducedBugsSkills_200/data/NSGAII/xcisco_reducedBugsSkills/";
		double[][] bestData = SolutionSelector.getBestSolutions(funFolder + "FUN.0", "");

		String fileChartContent = FileUtils.readFileToString(
				"/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/src/edu/birzeit/jmse/sbse/reporting/charts/chart.html");

		double[] maxObjectives = new double[Constants.numberOfObjectives];
		for (int j = 0; j < maxObjectives.length; j++) {
			maxObjectives[j] = 0;
		}

		// find max
		for (int i = 0; i < bestData.length; i++) {
			for (int j = 0; j < Constants.numberOfObjectives; j++) {
				if (bestData[i][j]< 0) {
					bestData[i][j] *= -1;
				}
				if (bestData[i][j] > maxObjectives[j]) {
					maxObjectives[j] = bestData[i][j];
				}
			}
		}

		// scale values
		double scale = maxObjectives[0] + 3;

		// scale best data
		for (int i = 0; i < bestData.length; i++) {
			for (int j = 0; j < Constants.numberOfObjectives; j++) {
			 bestData[i][j] = bestData[i][j] / maxObjectives[j]   * maxObjectives[0] ;
			}
		}

		for (int obj = 0; obj < Constants.numberOfObjectives; obj++) {
			String objValues = "";
			for (int i = 0; i < bestData.length; i++) {
				objValues += bestData[i][obj];
				if (i < bestData.length - 1) {
					objValues += " , ";
				}
			}
			fileChartContent = fileChartContent.replace("####OBJ" + (obj + 1) + "####", objValues);
			fileChartContent = fileChartContent.replace("//####AddSolutions#####", "chart.line(data" + (obj + 1)
					+ ").name('Solution" + (obj + 1) + "').markers(true);" + "\n//####AddSolutions#####");

		}

		FileUtils.writeStringToFile(fileChartContent, funFolder + "SolutionsChart.html");
	}

	// chart.line(data####).name('Solution####').markers(true);

}
