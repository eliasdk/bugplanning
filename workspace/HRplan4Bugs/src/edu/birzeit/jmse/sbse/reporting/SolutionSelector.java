package edu.birzeit.jmse.sbse.reporting;

import edu.birzeit.jmse.sbse.Constants;
import edu.birzeit.jmse.sbse.FileUtils;

public class SolutionSelector {

	public static void main(String[] args) {
		String funFolder = "/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/StudyOutput/xcisco_reducedBugsSkills_200/data/NSGAII/xcisco_reducedBugsSkills/";
		double [][]  bestData =  getBestSolutions(
				funFolder + "FUN.0",
				"");
		System.out.println(bestData);

	}

	static double[][] getBestSolutions( String funPath, String varPath) {

		int num = Constants.numberOfObjectives + 1 ;
		double[][]  funData = getFunDatafunContent(funPath);
		double [][]  bestData = new double [num][Constants.numberOfObjectives];
		
		for(int obj=0; obj<Constants.numberOfObjectives; obj++) {
			double []objMax = new double [Constants.numberOfObjectives];	
			for(int i=0; i<Constants.numberOfObjectives; i++) {
				objMax[i] = 100000000 ;
			}
			double solutionSum = 100000000;
			
			for (int s=0; s< funData.length-1 ; s++ ) {
				if (funData[s][obj]  ==  funData[funData.length-1][obj]  ) {
					 double sum = 0 ;
					 for(int j=0;j<Constants.numberOfObjectives; j++) {
						 if (funData[s][j] < 0 ) {
							 sum +=  1 - (funData[s][j]/funData[funData.length-1][j]) ;
						 }
						 else {
							 sum +=  1- funData[funData.length-1][j]/funData[s][j];
						 }
					 }
					 if (sum <solutionSum ) {
						 solutionSum = sum ;
						 for(int j=0;j<Constants.numberOfObjectives; j++) {
							 bestData[obj][j] = funData[s][j];
						 }		
					 }
				}
			}
			
		}
		double solutionSum = 100000000;
		for (int s=0; s< funData.length-1 ; s++ ) {
		 
				 double sum = 0 ;
				 for(int j=0;j<Constants.numberOfObjectives; j++) {
					 if (funData[s][j] < 0 ) {
						 sum +=  1 - (funData[s][j]/funData[funData.length-1][j]) ;
					 }
					 else {
						 sum +=  1- funData[funData.length-1][j]/funData[s][j];
					 }
				 }
				 if (sum <solutionSum ) {
					 solutionSum = sum ;
					 for(int j=0;j<Constants.numberOfObjectives; j++) {
						 bestData[num-1][j] = funData[s][j];
					 }		
				 }
 
		}
		return bestData;

	}

	static double[][] getFunDatafunContent(String funPath) {
		String funContent = FileUtils.readFileToString(funPath);
		String[] lines = funContent.split("\n");

		double[] maxObjectives = new double[Constants.numberOfObjectives];
		for (int j = 0; j < maxObjectives.length; j++) {
			maxObjectives[j] = 100000000;
		}
		
		double[][] data = new double[lines.length+1][Constants.numberOfObjectives];

		for (int i = 0; i < lines.length; i++) {
			String[] objectives = lines[i].split(" ");
			for (int j = 0; j < objectives.length; j++) {
				data[i][j] =  Double.parseDouble(objectives[j]);
				if (data[i][j]  < maxObjectives[j]  ) {
					maxObjectives[j] = data[i][j];
				}
			}
		}
		//last row is the max values
		for (int i = 0; i < lines.length; i++) {
			for (int j = 0; j < Constants.numberOfObjectives; j++) {
				data[lines.length][j] = maxObjectives[j] ;
			}
		}
		return data;

	}
}
