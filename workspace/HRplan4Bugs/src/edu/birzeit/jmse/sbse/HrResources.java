package edu.birzeit.jmse.sbse;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import edu.birzeit.jmse.jmetal.extension.MixedPermutationSolutionType;
import edu.birzeit.jmse.jmetal.extension.MixedPermutation;
import edu.birzeit.jmse.sbse.reader.DatasetReader;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.solutionType.PermutationSolutionType;
import jmetal.encodings.variable.Binary;
import jmetal.util.JMException;

abstract public class HrResources extends Problem {

	static double maxFitness = 0;

	public static final int SEVERITY = 0;
	public static final int PRIORITY = 1;
	public static final int ETA = 2;
	public static final int REQUIRED_SKILLES = 3;
	// public static final int BUG_TYPE = 4;

	public static final int Employee_Skills = 0;
	public int numberOfBugs_;
	public int numberOfEmployees_;
	public double[][] bugsDetailsMatrix_;
	public double[][] employeeDetailsMatrix_;
	public double totalNumberOfSeverity_ = 0;
	public double totalNumberOfPriority_ = 0;
	public String problemType ;

	int numberOfBits;
	int employeeBits;
	int globalSequence = 0;
	boolean isJDT = true;

	public HrResources(String solutionType, String bugsFilename, String developersFilename) throws Exception {

		problemType = solutionType;
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;

		problemName_ = "HrReseources";

		//solutionType_ = new BinarySolutionType(this);

	 
		if(bugsFilename.split("/")[bugsFilename.split("/").length-1].startsWith("rks")) {
			readDirectProblem(bugsFilename);
			System.out.println("R: Reading from file: "+bugsFilename);
		}
		else if(bugsFilename.split("/")[bugsFilename.split("/").length-1].startsWith("xcis")) {
			readDirectProblem(bugsFilename);
			System.out.println("C: Reading from file: "+bugsFilename);
		}
		else if(bugsFilename.split("/")[bugsFilename.split("/").length-1].startsWith("exp_")) {
			readDirectProblem(bugsFilename);
			System.out.println("C: Reading from file: "+bugsFilename);
		}
		else {
			readProblem(bugsFilename);
		}
		

		readEmplpyees(developersFilename);
		// for (int i = 0; i < numberOfEmployees_; i++) {
		// System.out.println(" Employee id : " + i + " " +
		// employeeDetailsMatrix_[i][0]);
		// }

		
		/*
		 * for (int i=0; i<bugsDetailsMatrix_.length; i++) { for(int j=0;
		 * j<bugsDetailsMatrix_[0].length; j++) {
		 * System.out.print(bugsDetailsMatrix_[i][j] + "\t"); }
		 * System.out.println("\n"); }
		 */

		if (bugsFilename.contains("JDT")) {
			isJDT = true;
		} else {
			isJDT = false;
		}
		
		try {
			if (solutionType.compareTo("Binary") == 0) {
				numberOfVariables_ = numberOfBugs_;
				if (numberOfEmployees_ == 8) {
					numberOfBits = 3 + Constants.sequenceBitSize;
					employeeBits = 3;
				} else if (numberOfEmployees_ == 16) {
					numberOfBits = 4 + Constants.sequenceBitSize;
					employeeBits = 4;
				} else if (numberOfEmployees_ == 32) {
					numberOfBits = 5 + Constants.sequenceBitSize;
					employeeBits = 5;
				} else if (numberOfEmployees_ == 64) {
					numberOfBits = 6 + Constants.sequenceBitSize;
					employeeBits = 6;
				} else {
					numberOfBits = 2 + Constants.sequenceBitSize;
					employeeBits = 2;
				}
				length_ = new int[numberOfVariables_];
				for (int i = 0; i < numberOfVariables_; i++) {
					length_[i] = numberOfBits;
				}
				System.out.println("Number of Bugs : " + numberOfBugs_);
				solutionType_ = new BinarySolutionType(this);
			} else if (solutionType.compareTo("MixedPermutation") == 0) {
				numberOfVariables_ = 1;
				length_ = new int[numberOfVariables_];
				length_[0] = this.numberOfBugs_;
				solutionType_ = new MixedPermutationSolutionType(this, this.numberOfEmployees_);
			} else {
				throw new JMException("Solution type invalid");
			}
		} catch (JMException e) {
			e.printStackTrace(); // To change body of catch statement use File |
									// Settings | File Templates.
		}


		// calculate bugs time, num of developer. and iteration time

	}

	protected FitnessValues getSolutionFitness(Solution solution) {
		// System.out.println("Constants.iterationWorkingHours " +
		// Constants.iterationWorkingHours);
		int maxRequiredTime = 0; // hours
		ArrayList<BugAssignment>[] developerAssignments=null ;
		FitnessValues fitnessValues = new FitnessValues();
		Variable variables[] = solution.getDecisionVariables();
		if(this.problemType .equals("Binary")) {
				developerAssignments = getBugBinaryDistribution(variables);
		}else if(this.problemType .equals("MixedPermutation")) {
			developerAssignments = getBugMixedPermutationDistribution(variables);
		}
	

		// System.out.println("--------------------------------------------");
		for (int i = 0; i < numberOfEmployees_; i++) {
			int developerMaxTime = 0;
			// System.out.print("DevId : " + i + " ( ");
			int developerPeriod = 0;
			boolean devAssignDone = false;
			for (int j = 0; j < developerAssignments[i].size(); j++) {
				int bugID = developerAssignments[i].get(j).getBugId();

				double actualFixTime = 0;

				actualFixTime = getDeveloperBugEffort(i, bugID) ;  
				// System.out.println("ETA = " + bugsDetailsMatrix_[bugID][2] + " ,
				// actualFixTime = " + actualFixTime);

				if ((developerPeriod + actualFixTime < Constants.iterationWorkingHours) && !devAssignDone) {
					developerPeriod += actualFixTime;
					fitnessValues.setNumberFixedBugsFitness(fitnessValues.getNumberFixedBugsFitness() + 1);
					
					//add bug creation time difference for aging fitness
					fitnessValues.setTimeDiff(fitnessValues.getTimeDiff()+ (int)bugsDetailsMatrix_[bugID][DatasetReader.CREATE_DATE] );
					//DatasetReader.printBugsDetails(bugsDetailsMatrix_);
					//System.out.println(bugID + " , " + bugsDetailsMatrix_[bugID][DatasetReader.SEVERITY] + " , " + bugsDetailsMatrix_[bugID][DatasetReader.PRIORITY]);
					if ((bugsDetailsMatrix_[bugID][DatasetReader.SEVERITY] >= 4.0)
							|| (bugsDetailsMatrix_[bugID][DatasetReader.PRIORITY] <= 2.0)) {
						fitnessValues.setHighSeverityFitness((int) (fitnessValues.getHighSeverityFitness()) + 1); // bugsDetailsMatrix_[bugID][SEVERITY]));
					}
					/*
					 * if (bugsDetailsMatrix_[bugID][DatasetReader.PRIORITY] <= 2) {
					 * fitnessValues.setHighPriorityFitness((int)
					 * (fitnessValues.getHighPriorityFitness()) + 1);//
					 * bugsDetailsMatrix_[bugID][PRIORITY])); }
					 */
					developerMaxTime += actualFixTime;
					// System.out.print("" + bugID + " , ");
				} else {
					devAssignDone = true;
					developerMaxTime += actualFixTime;
				}
			}
			// System.out.print(" ) ---- ");
			maxRequiredTime += developerMaxTime;
			// if (developerMaxTime > maxRequiredTime) {
			// maxRequiredTime = developerMaxTime;
			// }
		}

		// System.out.println("\nN:" + fitnessValues.getNumberFixedBugsFitness()
		// + " S: "
		// + fitnessValues.getHighSeverityFitness() + " P: " +
		// fitnessValues.getHighPriorityFitness());
		fitnessValues.setTimeToFixAllFitness(maxRequiredTime);
		return fitnessValues;
	}

	private double getDeveloperBugEffort(int DevId, int bugId) {

		double bugEffort = bugsDetailsMatrix_[bugId][DatasetReader.EFFORT];
		int startIdx = 0, endIdx = 0, idxDiff = 0;
		double devEffort = 0;

		if (isJDT) {
			startIdx = DatasetReader.DEV_JDT_START_SKILL_IDX;
			//endIdx = DatasetReader.DEV_JDT_END_SKILL_IDX;
			idxDiff = DatasetReader.BUG_JDT_START_SKILL_IDX - DatasetReader.DEV_JDT_START_SKILL_IDX;
		} else {
			startIdx = DatasetReader.DEV_PLATFORM_START_SKILL_IDX;
			//endIdx = DatasetReader.DEV_PLATFORM_END_SKILL_IDX;
			idxDiff = DatasetReader.BUG_PLATFORM_START_SKILL_IDX - DatasetReader.DEV_PLATFORM_START_SKILL_IDX;

		}

		endIdx =  employeeDetailsMatrix_[1].length - 2 ;
		for (int skillIdx = startIdx; skillIdx <= endIdx; skillIdx++) {
			double skillWeight = employeeDetailsMatrix_[DevId][skillIdx];
			if (skillWeight == 0) {
				skillWeight = 0.1;
			}
			
			devEffort += bugEffort * bugsDetailsMatrix_[bugId][skillIdx + idxDiff] / skillWeight; // 0.01 to avoid
																									// devide by zero
		}

		return devEffort;

	}

	/*
	 * private double getDeveloperBugTypeSkill(int developerID, int bugId) { int
	 * bugType = (int) bugsDetailsMatrix_[bugId][BUG_TYPE]; return
	 * employeeDetailsMatrix_[developerID][bugType]; }
	 */

	public void readDirectProblem(String fileName) throws Exception {
		bugsDetailsMatrix_ = DatasetReader.loadDirectBugProperties(fileName);
		numberOfBugs_ = bugsDetailsMatrix_.length;
	} // readProblem
	
	public void readProblem(String fileName) throws Exception {
		double[][] basicBugsMatrix = DatasetReader.readDataFile(fileName);
		bugsDetailsMatrix_ = DatasetReader.fillBugProperties(basicBugsMatrix);
		numberOfBugs_ = bugsDetailsMatrix_.length;
	} // readProblem

	public void readEmplpyees(String fileName) throws IOException {

		employeeDetailsMatrix_ = DatasetReader.readDataFile(fileName);
		numberOfEmployees_ = employeeDetailsMatrix_.length;

	} // readEmplpyees

	BugAssignment getBugBinaryDeveloper(int bugId, Variable variable) {
		BugAssignment bugAssignment = new BugAssignment();
		for (int i = 0; i < numberOfBits; i++) {

			if (i < employeeBits) { // id
				if (((Binary) variable).getIth(i)) {
					bugAssignment.setDeveloperId((int) (bugAssignment.getDeveloperId() + Math.pow(2, i)));

				}
			} else {
				if (((Binary) variable).getIth(i)) {
					bugAssignment.setSequence((int) (bugAssignment.getSequence() + Math.pow(2, i - employeeBits)));
				}
			}
		}

		bugAssignment.setBugId(bugId);
		return bugAssignment;

	}

	private String getVariableBinaryString(Variable[] variables) {
		String binStr = "";
		for (int varIdx = 0; varIdx < variables.length; varIdx++) {
			if (varIdx > 0) {
				binStr += "-";
			}
			binStr += ((Binary) (variables[varIdx])).toString();
		}
		return binStr;
	}

	public ArrayList<BugAssignment>[] getBugBinaryDistribution(Variable[] variables) {

		ArrayList<BugAssignment>[] developerAssignments = new ArrayList[numberOfEmployees_];
		for (int i = 0; i < developerAssignments.length; i++) {
			developerAssignments[i] = new ArrayList<BugAssignment>();
		}
		// System.out.println(" developerAssignments = " +developerAssignments);

		// String binStr = getVariableBinaryString (variables) ;
		// System.out.println(binStr);

		for (int bugIdx = 0; bugIdx < variables.length - Constants.varsUsedForSequence; bugIdx++) {
			BugAssignment bugAssignment = getBugBinaryDeveloper(bugIdx, variables[bugIdx]);
			// System.out.println(bugAssignment);
			developerAssignments[bugAssignment.getDeveloperId()].add(bugAssignment);
		}

		if (Constants.varsUsedForSequence > 0) {
			String sequenceBinStr = "";
			for (int var = variables.length - Constants.varsUsedForSequence; var < variables.length; var++) {
				sequenceBinStr += ((Binary) (variables[var])).toString();
			}
			globalSequence = Integer.parseInt(sequenceBinStr, 2);
			// System.out.println("globalSequence ......... " + globalSequence);
		}

		if (Constants.sequenceBitSize > 0) {
			for (int i = 0; i < numberOfEmployees_; i++) {
				Collections.sort(developerAssignments[i], BugAssignment.BugAssignmentComparator);
			}
		} else {
			Random r = new Random(globalSequence);

			List<Integer> uniqueNumbers = new ArrayList<Integer>();
			Integer nextInt;
			while (uniqueNumbers.size() < numberOfBugs_) {
				nextInt = r.nextInt(numberOfBugs_);
				if (!uniqueNumbers.contains(nextInt))
					uniqueNumbers.add(nextInt);
			}

			for (int i = 0; i < numberOfEmployees_; i++) {
				BugAssignment.setUniqueNumbers(uniqueNumbers);
				Collections.sort(developerAssignments[i], BugAssignment.BugAssignmentGlobalComparator);
			}

		}
		return developerAssignments;

	}
	 
	public ArrayList<BugAssignment>[] getBugMixedPermutationDistribution(Variable[] variables) {

		ArrayList<BugAssignment>[] developerAssignments = new ArrayList[numberOfEmployees_];
		for (int i = 0; i < developerAssignments.length; i++) {
			developerAssignments[i] = new ArrayList<BugAssignment>();
		}
		// System.out.println(" developerAssignments = " +developerAssignments);

		// String binStr = getVariableBinaryString (variables) ;
		// System.out.println(binStr);

		for (int bugIdx = 0; bugIdx < this.numberOfBugs_; bugIdx++) {
		
			BugAssignment bugAssignment = new BugAssignment();
			bugAssignment.setBugId(bugIdx);
			bugAssignment.setDeveloperId(((MixedPermutation)variables[0]).vector_[bugIdx][0]);
			bugAssignment.setSequence(((MixedPermutation)variables[0]).vector_[bugIdx][1]);
			 
			// System.out.println(bugAssignment);
			developerAssignments[bugAssignment.getDeveloperId()].add(bugAssignment);
		}

		return developerAssignments;

	}
}
