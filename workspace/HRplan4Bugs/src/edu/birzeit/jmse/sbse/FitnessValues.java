package edu.birzeit.jmse.sbse;

public class FitnessValues {
	
	private int highPriorityFitness = 0;
	private int highSeverityFitness = 0;
	private int numberFixedBugsFitness = 0;
	private int timeToFixAllFitness =10000000;
	private int timeDiff = 0;
	
	public int getTimeDiff() {
		return timeDiff;
	}
	public void setTimeDiff(int timeDiff) {
		this.timeDiff = timeDiff;
	}
	public int getTimeToFixAllFitness() {
		return timeToFixAllFitness;
	}
	public void setTimeToFixAllFitness(int timeToFixAllFitness) {
		this.timeToFixAllFitness = timeToFixAllFitness;
	}
	public int getHighPriorityFitness() {
		return highPriorityFitness;
	}
	public void setHighPriorityFitness(int highPriorityFitness) {
		this.highPriorityFitness = highPriorityFitness;
	}
	public int getHighSeverityFitness() {
		return highSeverityFitness;
	}
	public void setHighSeverityFitness(int highSeverityFitness) {
		this.highSeverityFitness = highSeverityFitness;
	}
	public int getNumberFixedBugsFitness() {
		return numberFixedBugsFitness;
	}
	public void setNumberFixedBugsFitness(int numberFixedBugsFitness) {
		this.numberFixedBugsFitness = numberFixedBugsFitness;
	}
	void incrementHighPriorityFitness() {
		highPriorityFitness++;
	}
	void  incrementHighSeverityFitness() {
		highSeverityFitness++;
	}
	void incrementNumberFixedBugsFitness() {
		numberFixedBugsFitness++;
	}
	

}
