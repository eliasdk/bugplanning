package edu.birzeit.jmse.sbse;

import java.util.ArrayList;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;

public class SoHrResources extends HrResources {

 
	public SoHrResources(String solutionType, String bugsFilename, String developersFilename) throws Exception {
		super( solutionType, bugsFilename,  developersFilename);
		this.numberOfObjectives_ = 1;
	}
	
	@Override
	public void evaluate(Solution solution) throws JMException {
		 

		FitnessValues fitnessValues = getSolutionFitness ( solution);

		double fitness = fitnessValues.getNumberFixedBugsFitness() / numberOfBugs_ + fitnessValues.getHighPriorityFitness() / totalNumberOfPriority_
				+ fitnessValues.getHighSeverityFitness() / totalNumberOfSeverity_;
		
		if (maxFitness < fitness) {
			maxFitness = fitness;
			System.out.println("numberOfBugs_ " + fitnessValues.getNumberFixedBugsFitness() + " totalNumberOfPriority_ " + fitnessValues.getHighPriorityFitness()  + " sev  " + fitnessValues.getHighSeverityFitness() + "  time " + fitnessValues.getTimeToFixAllFitness() );
		}
		solution.setObjective(0, 3 - fitness);
	}
	
	
 
}
