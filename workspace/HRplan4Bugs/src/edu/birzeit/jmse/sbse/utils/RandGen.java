package edu.birzeit.jmse.sbse.utils;



import java.util.Date;
import java.util.Random;

public class RandGen {

	public static void main(String[] args) {
		Date date = new Date();
		Random generator = new Random(date.getTime());
		

		int[][] weights = { { 1145, 33 }, { 820, 41 }, { 750, 17 }, { 702, 12 }, { 604, 23 }, { 590, 13 }, { 583, 35 },
				{ 513, 37 }, { 480, 9 }, { 467, 15 }, { 433, 22 }, { 422, 11 }, { 415, 37 }, { 409, 19 }, { 400, 17 },
				{ 394, 26 }, { 382, 33 }, { 377, 35 }, { 360, 14 }, { 359, 19 }, { 344, 40 }, { 333, 24 }, { 320, 14 },
				{ 266, 21 }, { 246, 7 }, { 221, 13 }, { 217, 21 }, { 201, 30 }, { 167, 23 }, { 143, 14 }, { 112, 16 },
				{ 99, 22 }, { 95, 21 }, { 94, 38 }, { 89, 21 }, { 87, 17 }, { 80, 23 }, { 77, 24 }, { 72, 31 },
				{ 66, 15 }, { 61, 12 }, { 55, 7 }, { 48, 14 }, { 47, 24 }, { 42, 36 }, { 36, 41 }, { 33, 19 },
				{ 33, 27 }, { 32, 29 }, { 31, 13 }, { 31, 15 }, { 30, 33 }, { 30, 22 }, { 29, 19 }, { 29, 30 },
				{ 29, 15 }, { 45, 44 }, { 45, 14 }, { 42, 7 }, { 40, 9 }, { 40, 21 }, { 40, 12 }, { 40, 17 },
				{ 39, 33 }, { 39, 40 }, { 39, 31 }, { 39, 29 }, { 37, 11 }, { 35, 32 }, { 35, 12 }, { 35, 22 },
				{ 35, 35 }, { 32, 12 }, { 31, 9 }, { 31, 4 }, { 30, 16 }, { 27, 32 }, { 27, 41 }, { 27, 40 },
				{ 27, 25 }, { 27, 26 }, { 27, 21 }, { 25, 17 }, { 24, 15 }, { 24, 31 }, { 24, 8 }, { 21, 11 },
				{ 21, 21 }, { 21, 3 }, { 20, 6 }, { 20, 21 }, { 18, 15 }, { 17, 16 }, { 15, 23 }, { 15, 27 }, { 15, 4 },
				{ 9, 12 }, { 4, 30 }, { 3, 14 }, { 3, 7 }

		};

		for (int customer = 0; customer < 100; customer++) {
			int len = weights[customer][1];;
			int weight = weights[customer][0];
			int[] arr = new int[len];

			int filled = 0;
			while (true) {
				int num = generator.nextInt(119);
				if (filled == 0) {
					arr[filled] = num;
					filled++;
					if (filled == len) {
						break;
					}
					continue;
				}
				for (int i = 0; i < filled; i++) {
					if (arr[i] == num) {
						break;
					}
				}
				arr[filled] = num;
				filled++;
				if (filled == len) {
					break;
				}
			}

			System.out.print(weight + " " + len);
			for (int i = 0; i < len; i++) {
				System.out.print(" " + (arr[i] + 1));
			}

			System.out.println("");

		}
	}

}
