package edu.birzeit.jmse.sbse.utils;

import edu.birzeit.jmse.sbse.FileUtils;

public class RemoveDominated {

	public static void main(String[] args) {
		String folderPath = "/Users/EliasK/GoogleDrive/BZUmaster/dev/repos/sbse/HrResourcesSBSE/src/edu/birzeit/jmse/sbse/utils/";
		String csv100_4 = folderPath + "NSGA100_4.csv";
		String csv100_8 = folderPath + "NSGA100_8.csv";
		String csv50_4 = folderPath + "NSGA50_4.csv";
		String csv50_8 = folderPath + "NSGA50_8.csv";
	 
		
		String data100_4 = FileUtils.readFileToString (csv100_4);
		String data100_8 = FileUtils.readFileToString (csv100_8);
		String data50_4 = FileUtils.readFileToString (csv50_4);
		String data50_8 = FileUtils.readFileToString (csv50_8);
		
		String out = removeDominated(data100_4);
		FileUtils.writeStringToFile(out, csv100_4+"nd.csv");
		
		  out = removeDominated(data100_8);
		FileUtils.writeStringToFile(out, csv100_8+"nd.csv");
		
		  out = removeDominated(data50_4);
		FileUtils.writeStringToFile(out, csv50_4+"nd.csv");
		
		  out = removeDominated(data50_8);
		FileUtils.writeStringToFile(out, csv50_8+"nd.csv");
	}
	
	static public String removeDominated(String data) {
		String out = "";
		String outTemp = "";
		String dataArray [] = data.split("\n");
	   int  numBug []  = new int [dataArray.length];
	   int  numSev []  = new int [dataArray.length];
	
	   for (int i=0; i<dataArray.length; i++){
		   numBug[i] = Integer.parseInt(dataArray[i].split(",")[0]);
		   numSev[i] = Integer.parseInt(dataArray[i].split(",")[1]);
		  // System.out.println(numBug[i] + ", " + numSev[i] );
	   }
	   for (int i=0; i<dataArray.length; i++){
		   boolean nondominated = true ;
		   for (int j=0; j<dataArray.length; j++){
			   if (i==j) {
				   continue;
			   }
			   if ((numBug[j] > numBug[i]) && ( numSev[j] >=numSev[i]))  {
				   nondominated = false;
			   }
			   else  if ((numBug[j] == numBug[i]) && ( numSev[j] >numSev[i]))  {
				   nondominated = false;
			   }
			   
			   
		   }
		   if (nondominated) {
			  
			   if (outTemp.split("#"+numBug[i] + " , " + numSev[i] + "#").length <=1 ) {
				   out += numBug[i] + " , " + numSev[i] + "\r\n" ;
			   }
			   
			  
			   
			   outTemp += "#"+numBug[i] + " , " + numSev[i] + "#" ;
		   }
	   }
	return out;
		
	}

}
