package edu.birzeit.jmse.sbse.utils;



public class NonDominatedContent {

	String varContent="";
	String funContent="";
    int numberOfObjectives;
	
	
	public int getNumberOfObjectives() {
		return numberOfObjectives;
	}
	public void setNumberOfObjectives(int numberOfObjectives) {
		this.numberOfObjectives = numberOfObjectives;
	}
	public String getVarContent() {
		return varContent;
	}
	public void setVarContent(String varContent) {
		this.varContent = varContent;
	}
	public String getFunContent() {
		return funContent;
	}
	public void setFunContent(String funContent) {
		this.funContent = funContent;
	}

	
	
	
}
