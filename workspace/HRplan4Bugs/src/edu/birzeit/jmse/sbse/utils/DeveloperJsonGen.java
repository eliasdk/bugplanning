package edu.birzeit.jmse.sbse.utils;

import edu.birzeit.jmse.sbse.reader.DatasetReader;

public class DeveloperJsonGen {

	public static void main(String[] args) throws Exception {
		String developersFilePath = "/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/dataset/exp_developers.txt";
		double[][] bugsFileContent = DatasetReader.readDataFile(developersFilePath);

		for (int i = 0; i < bugsFileContent.length; i++) {
			int dev = (int) bugsFileContent[i][0];
		 

			System.out.println(" { 'name': 'Developer" + dev + "' , 'weight': " + bugsFileContent[i][1]
					+ " , 'Dashboard': " + bugsFileContent[i][2] + " , 'Spring': " + bugsFileContent[i][3]
					+ " , 'Messaging': " + bugsFileContent[i][4] + " , 'Database': " + bugsFileContent[i][5]
					+ " , 'Wage': " + bugsFileContent[i][6] + ", 'tasks': []},");

		}
	}

}
