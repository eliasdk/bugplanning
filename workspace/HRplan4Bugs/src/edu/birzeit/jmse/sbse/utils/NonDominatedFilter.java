package edu.birzeit.jmse.sbse.utils;

import edu.birzeit.jmse.sbse.FileUtils;

public class NonDominatedFilter {

	private static int numObj;
 
	
	
 
	public static void main3(String[] args) {
		
		String rootDir = System.getProperty("user.dir");
		String experimentResultFolder = rootDir +  "/ExperimentResults" ;
		String funName = experimentResultFolder + "/FUN_ALL"  ;
		String varName = experimentResultFolder + "/VAR_ALL"  ;
        String outFolder = experimentResultFolder + "/Dominated/Splited" ;
		createNominated2Obj(funName, varName, outFolder) ;
		
		 funName = experimentResultFolder + "/NSGAII/FUN.0"  ;
		 varName = experimentResultFolder + "/NSGAII/VAR.0" ;
		 outFolder = experimentResultFolder + "/Dominated/Splited/NSGAII" ;
		createNominated2Obj(funName, varName,outFolder ) ;
	}
		
	public static void createNominated2Obj(String funName, String varName, String outFolder) {
		String rootDir = System.getProperty("user.dir");
		String experimentResultFolder = rootDir +  "/ExperimentResults" ;
 
		String outVarName = experimentResultFolder + "/temp/VAR_ALL"  ;
		
		String outFile = outFolder + "/NUM_PRI" ;
		NonDominatedContent content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 0, 1));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
	
		outFile = outFolder + "/NUM_Time" ;
		content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 0, 3));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
		outFile = outFolder+ "/NUM_AGE" ;
		content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 0, 2));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
		outFile = outFolder + "/PRI_TIME" ;
		content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 1, 3));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
		outFile = outFolder +"/PRI_AGE" ;
		content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 1, 2));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
		outFile = outFolder +"/TIME_AGE" ;
		content = new NonDominatedContent();
		content.setFunContent(keepObjectives (funName, 3, 2));
		content.setVarContent(FileUtils.readFileToString(varName));
		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFile);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		
			
	}
	public static String keepObjectives(String originalFilePath, int obj1, int obj2) {
		 
		String originalContent = FileUtils.readFileToString(originalFilePath);
		 String newContent = "";
		 String funs[] = originalContent.split("\n");
		 for (String fun: funs ) {
			 String [] objs = fun.split(" ");
			 newContent += objs[obj1] + " " + objs[obj2] + "\n" ;
		 }
		 return newContent;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String rootDir = System.getProperty("user.dir");
		String experimentResultFolder = rootDir +  "/ExperimentResults" ;
		/*
		for (int i=1; i<=14; i++) {
			NonDominatedContent content = new NonDominatedContent();
			String funName = experimentResultFolder + "/FUN_User"+i ;
			String varName = experimentResultFolder + "/VAR_User"+i ;
			String outFunName = experimentResultFolder + "/Dominated/FUN_User"+i ;
			String outVarName = experimentResultFolder + "/Dominated/VAR_User"+i ;
			System.out.println(funName);
			content.setFunContent(FileUtils.readFileToString(funName));
			content.setVarContent(FileUtils.readFileToString(varName));
	
			content = NonDominatedFilter.removeDominated(content);
			FileUtils.writeStringToFile(content.getFunContent(), outFunName);
			FileUtils.writeStringToFile(content.getVarContent(), outVarName);
		}
		*/
		
		NonDominatedContent content = new NonDominatedContent();
		String funName = experimentResultFolder + "/FUN_ALL2"  ;
		String varName = experimentResultFolder + "/VAR_ALL2"  ;
		String outFunName = experimentResultFolder + "/Dominated/FUN_ALL2"  ;
		String outVarName = experimentResultFolder + "/Dominated/VAR_ALL2"  ;
		System.out.println(funName);
		content.setFunContent(FileUtils.readFileToString(funName));
		content.setVarContent(FileUtils.readFileToString(varName));

		content = NonDominatedFilter.removeDominated(content);
		FileUtils.writeStringToFile(content.getFunContent(), outFunName);
		FileUtils.writeStringToFile(content.getVarContent(), outVarName);
	}

	private static NonDominatedContent removeDominated(NonDominatedContent content) {

		String funs[] = content.getFunContent().split("\n");
		String vars[] = content.getVarContent().split("\n");

		content.setNumberOfObjectives(funs[0].trim().split(" ").length);
		numObj = content.getNumberOfObjectives();
		System.out.println("Num of objectives = " + content.getNumberOfObjectives());
		double[][] vals = new double[vars.length][content.getNumberOfObjectives()];
		int nonDominated[] = new int[vars.length];

		for (int i = 0; i < funs.length; i++) {
			for (int j = 0; j < numObj; j++) {
				System.out.println(funs[i]);
				vals[i][j] = getFitnessValues(funs[i])[j];
			}
		}
		for (int i = 0; i < funs.length; i++) {
			nonDominated[i] = 1;
			for (int j = 0; j < funs.length; j++) {
				if (i == j) {
					continue;
				}
				 
				if (CompareDominateSolution(vals[j], vals[i]) == 1) {
					nonDominated[i] = 0;
					break;
				}
				else if (CompareDominateSolution(vals[j], vals[i]) == 0) {
					if(i>j) {
						nonDominated[i] = 0;
						break;
					}
				}
			}
		}
		NonDominatedContent newContent = new NonDominatedContent();
		for (int i = 0; i < funs.length; i++) {
			if (nonDominated[i] == 1) {
				newContent.setFunContent(newContent.getFunContent() + funs[i].trim() + "\n");
				newContent.setVarContent(newContent.getVarContent() + vars[i].trim() + "\n");
			}
		}

		return newContent;
	}
	
	
	private static double[] getFitnessValues(String strVal) {
		double[] funReading = new double[numObj];
		for (int j = 0; j < numObj; j++) {
			funReading[j] = Double.parseDouble(strVal.trim().split(" ")[j]);
		}
		return funReading;
	}

	private static int CompareDominateSolution(double[] s1, double[] s2) {

		for (int i = 0; i < numObj; i++) {
			if (s1[i] > s2[i]) {
				return -1;
			}
			boolean equalSol = false;
			
			if (s1[i] == s2[i]) {
				equalSol = true;
				for (int j = 0; j < numObj; j++) {
					if (s1[j] != s2[j]) {
						equalSol = false;
					}
				}
			}
			if (equalSol) {
				return 0;
			}

			boolean dominantSol = true;
			if (s1[i] <= s2[i]) {
				for (int j = 0; j < numObj; j++) {
					if (s1[j] > s2[j]) {
						dominantSol = false;
					}
				}
			}
			if (dominantSol) {
				return 1;
			}
		}

		System.out.println("*********** Should not reach here **************");
		return -1;
	}
	
	

}
