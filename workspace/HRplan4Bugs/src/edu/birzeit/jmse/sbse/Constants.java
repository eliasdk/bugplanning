package edu.birzeit.jmse.sbse;

public class Constants {
	public static int iterationWorkingDays = 5;
	public static int workingHoursPerDay   = 8 ;
	public static int iterationWorkingHours = iterationWorkingDays*workingHoursPerDay;
	public static int sequenceBitSize   = 4 ;
	public static int varsUsedForSequence = 0;

    public static String [] developersNames = {"Ahmad Yousef", "Tala Noor" , "Murad Shaheen", "Khalid Khalil", "Miral Daoud",  "Omar Shami", "Ayman Hussain", "Emad Wahbeh" };

    public static String iterationStartDate= "2017-12-01";
    
    public static  int numberOfObjectives = 4 ;
    
}
