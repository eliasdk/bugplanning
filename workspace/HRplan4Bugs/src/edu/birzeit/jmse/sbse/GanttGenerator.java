package edu.birzeit.jmse.sbse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;

public class GanttGenerator {

	private static long hourtime[] = { 0, 3600000, 7200000, 10800000, 14400000, 18000000, 21600000, 25200000, 86400000,
			90000000, 93600000, 97200000, 100800000, 104400000, 108000000, 111600000, 172800000, 176400000, 180000000,
			183600000, 187200000, 190800000, 194400000, 198000000, 259200000, 262800000, 266400000, 270000000,
			273600000, 277200000, 280800000, 284400000, 345600000, 349200000, 352800000, 356400000, 360000000,
			363600000, 367200000, 370800000 };

	public static void main(String[] args) throws ParseException {
		String dateStr = Constants.iterationStartDate;
		String DEFAULT_PATTERN = "yyyy-MM-dd";
		DateFormat formatter = new SimpleDateFormat(DEFAULT_PATTERN);
		Date myDate = formatter.parse(dateStr);
		System.out.println(myDate + "------" + myDate.getDay());
		Date loopDate = myDate;
		String startDatestr = "" + (loopDate.getMonth() + 1) + "/" + (loopDate.getDate()) + "/"
				+ (loopDate.getYear() + 1900);
		System.out.println(startDatestr);

	}

	public static void generateGantt(HrResources problem, SolutionSet population) throws ParseException {
		int totalNumberOfBugs =0;
		int s5=0, s4=0, s3=0, s2=0, s1=0;
		int p1=0, p2=0, p3=0, p4=0, p5=0;
		
		String taskDetails = "";

		ArrayList<BugAssignment>[] developerAssignments = problem
				.getBugBinaryDistribution(population.get(0).getDecisionVariables());

		for (int i = 0; i < problem.numberOfEmployees_; i++) {

			String dateStr = Constants.iterationStartDate;
			String DEFAULT_PATTERN = "yyyy-MM-dd";
			DateFormat formatter = new SimpleDateFormat(DEFAULT_PATTERN);
			Date startDate = formatter.parse(dateStr);
			int dateIdx = startDate.getDay() * 8; // index from the hour time
													// array
			System.out.println(" Date Idx = " + dateIdx);
			int developerPeriod = 0;

			Date loopDate = startDate;

			for (int j = 0; j < developerAssignments[i].size(); j++) {
				int bugID = developerAssignments[i].get(j).getBugId();
				int bugETA = (int) problem.bugsDetailsMatrix_[bugID][problem.ETA];
				int requiredSkill = (int) problem.bugsDetailsMatrix_[bugID][problem.REQUIRED_SKILLES];
				int actualFixTime = 0;
				int developerSkill = (int) problem.employeeDetailsMatrix_[i][problem.Employee_Skills];
				actualFixTime = bugETA * requiredSkill / developerSkill;

				if (developerPeriod + actualFixTime < Constants.iterationWorkingHours) {
					developerPeriod += actualFixTime;
					
					////// get counters ///
					totalNumberOfBugs++;
					if      ( problem.bugsDetailsMatrix_[bugID][problem.SEVERITY] ==  5) {s5++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.SEVERITY] ==  4) {s4++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.SEVERITY] ==  3) {s3++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.SEVERITY] ==  2) {s2++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.SEVERITY] ==  1) {s1++;}
					
					if      ( problem.bugsDetailsMatrix_[bugID][problem.PRIORITY] == 5 ) {p5++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.PRIORITY] == 4 ) {p4++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.PRIORITY] == 3 ) {p3++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.PRIORITY] == 2 ) {p2++;}
					else if ( problem.bugsDetailsMatrix_[bugID][problem.PRIORITY] == 1 ) {p1++;}
					////////////////////

					Date thisBugStartDate = loopDate;
					long timeDiff;

					if (actualFixTime + dateIdx < 40) {
						timeDiff = hourtime[actualFixTime + dateIdx] - hourtime[dateIdx];
						dateIdx = dateIdx + actualFixTime;

					} else {
						timeDiff = hourtime[39] + 3600000 - hourtime[dateIdx] + 64 * 3600000
								+ hourtime[actualFixTime + dateIdx - 40];
						dateIdx = actualFixTime + dateIdx - 40;

					}

					Date thisBugEndDate = new Date();
					thisBugEndDate.setTime(loopDate.getTime() + timeDiff);

					String startDatestr = "" + (thisBugStartDate.getMonth() + 1) + "/" + (thisBugStartDate.getDate())
							+ "/" + (thisBugStartDate.getYear() + 1900);

					String endDatestr = "" + (thisBugEndDate.getMonth() + 1) + "/" + (thisBugEndDate.getDate()) + "/"
							+ (thisBugEndDate.getYear() + 1900);

					System.out.println(" Developer " + i + " Bug " + bugID + " Hours = " + actualFixTime
							+ " Date Idx = " + dateIdx + " Period : " + startDatestr + " - " + endDatestr);

					loopDate.setTime(thisBugEndDate.getTime());
					
					taskDetails += "g.AddTaskItem(new JSGantt.TaskItem(2,   'Bug-"+bugID+"',    '" + startDatestr+"', '"+ endDatestr+"', '099ce6', '', 0, '"+ Constants.developersNames[i] +"',    0, 0, 0, 1,122));\r\n";
					  

				} else {
					break;
				}

				
			}
		}
		 
		/// fill values in file
		String planHtmlStr = FileUtils.readFileToString ("GanttPlan/HrResources.html") ;
		planHtmlStr = planHtmlStr.replaceAll("#####TASKS#####", ""+taskDetails);
		planHtmlStr = planHtmlStr.replaceAll("#####BUGS#####", ""+totalNumberOfBugs);
		planHtmlStr = planHtmlStr.replaceAll("#####S5#####", ""+s5);
		planHtmlStr = planHtmlStr.replaceAll("#####S4#####", ""+s4);
		planHtmlStr = planHtmlStr.replaceAll("#####S3#####", ""+s3);
		planHtmlStr = planHtmlStr.replaceAll("#####S2#####", ""+s2);
		planHtmlStr = planHtmlStr.replaceAll("#####S1#####", ""+s1);
		planHtmlStr = planHtmlStr.replaceAll("#####P5#####", ""+p5);
		planHtmlStr = planHtmlStr.replaceAll("#####P4#####", ""+p4);
		planHtmlStr = planHtmlStr.replaceAll("#####P3#####", ""+p3);
		planHtmlStr = planHtmlStr.replaceAll("#####P2#####", ""+p2);
		planHtmlStr = planHtmlStr.replaceAll("#####P1#####", ""+p1);
		
		FileUtils.writeStringToFile(planHtmlStr, "GanttPlan/Plan.html");
		
	}

}
