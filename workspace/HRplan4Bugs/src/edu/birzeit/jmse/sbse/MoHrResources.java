package edu.birzeit.jmse.sbse;

import edu.birzeit.jmse.sbse.FitnessValues;
import edu.birzeit.jmse.sbse.HrResources;
import jmetal.core.Solution;
import jmetal.util.JMException;

public class MoHrResources extends HrResources {

	static  boolean arrayLoaded = false;
	static public double[][] objectives;
	static public int sCount=0;
	static public String [] solutions = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""} ;
	static public String [] vars =  {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""} ;
    static public String funs[] = null;

	FitnessValues bestNumber = new FitnessValues();
	FitnessValues bestSeverity = new FitnessValues();
	FitnessValues bestPriority = new FitnessValues();

	public MoHrResources(String solutionType, String bugsFilename, String developersFilename) throws Exception {
		super(solutionType, bugsFilename, developersFilename);
		this.numberOfObjectives_ = Constants.numberOfObjectives;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {

		FitnessValues fitnessValues = getSolutionFitness(solution);

		double o1NumberofFixedBugs = fitnessValues.getNumberFixedBugsFitness();
		double o2NumberHighPriority = fitnessValues.getHighPriorityFitness();
		double o3NumberHighSeverity = fitnessValues.getHighSeverityFitness();
		double o3AgingTime = fitnessValues.getTimeDiff();
		double o4TimeToFixAllFitness = fitnessValues.getTimeToFixAllFitness();

		if ((o1NumberofFixedBugs > bestNumber.getNumberFixedBugsFitness())) {
			bestNumber = fitnessValues;
		} else if ((o1NumberofFixedBugs == bestNumber.getNumberFixedBugsFitness())) {
			if ((o2NumberHighPriority >= bestNumber.getHighPriorityFitness())
					&& (o3NumberHighSeverity >= bestSeverity.getHighSeverityFitness())) {
				bestNumber = fitnessValues;
			}
		}

		if (o2NumberHighPriority > bestPriority.getHighPriorityFitness()) {
			bestPriority = fitnessValues;
		} else if (o2NumberHighPriority == bestPriority.getHighPriorityFitness()) {
			if (((o1NumberofFixedBugs >= bestPriority.getNumberFixedBugsFitness()))
					&& (o3NumberHighSeverity >= bestPriority.getHighSeverityFitness())) {
				bestPriority = fitnessValues;
			}
		}

		if (o3NumberHighSeverity > bestSeverity.getHighSeverityFitness()) {
			bestSeverity = fitnessValues;
		} else if (o3NumberHighSeverity == bestSeverity.getHighSeverityFitness()) {
			if ((o2NumberHighPriority >= bestSeverity.getHighPriorityFitness())
					&& ((o1NumberofFixedBugs >= bestSeverity.getNumberFixedBugsFitness()))) {

				bestSeverity = fitnessValues;
			}
		}
/*
		if (!arrayLoaded) {
			String funStr = FileUtils.readFileToString(
					"/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/ExperimentResults/Fun_ALL2");
			funs=  funStr.split("\n");
			int numObj = 4;
			objectives = new double[funs.length][numObj];
			for (int i = 0; i < funs.length; i++) {
				for (int j = 0; j < numObj; j++) {
					objectives[i][j] = Double.parseDouble(funs[i].trim().split(" ")[j]);
				}
			}

			arrayLoaded = true;
		}

		for (int s = 0; s < objectives.length; s++) {
			if (    (o1NumberofFixedBugs >20) && 
					(Math.abs (-1*o1NumberofFixedBugs - objectives[s][0]) <= 0) &&  
					(Math.abs (-1*o3NumberHighSeverity - objectives[s][1]) <= 0) &&
					(Math.abs (-1*o3AgingTime - objectives[s][2]) <= 200) &&
					(Math.abs (o4TimeToFixAllFitness - objectives[s][3]) <= 50) 

					) {
				sCount++;
				solutions[s] += -1 * o1NumberofFixedBugs + " " + -1 * o3NumberHighSeverity + " " + -1 * o3AgingTime
						+ " " + o4TimeToFixAllFitness +"\n";
				vars[s] += solution.getDecisionVariables()[0].toString() + "\n";
				 
			}
			
		}
		*/
		// System.out.println("maxRequiredTime ..... "+ o4TimeToFixAllFitness);
		solution.setObjective(0, -1 * o1NumberofFixedBugs);
		// solution.setObjective(1, -1 * o2NumberHighPriority);
		solution.setObjective(1, -1 * o3NumberHighSeverity);
		solution.setObjective(2, -1 * o3AgingTime);
		solution.setObjective(3, o4TimeToFixAllFitness);

		// solution.setLeftTime(o4TimeToFixAllFitness);

	}

	public String toString() {
		String result = " Best solutions: \n";
		result += bestNumber.getNumberFixedBugsFitness() + " " + bestNumber.getHighPriorityFitness() + " "
				+ bestNumber.getHighSeverityFitness() + "\n";
		result += bestPriority.getNumberFixedBugsFitness() + " " + bestPriority.getHighPriorityFitness() + " "
				+ bestPriority.getHighSeverityFitness() + "\n";
		result += bestSeverity.getNumberFixedBugsFitness() + " " + bestSeverity.getHighPriorityFitness() + " "
				+ bestSeverity.getHighSeverityFitness() + "\n";

		return result;

	}

}
