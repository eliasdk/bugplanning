
package edu.birzeit.jmse.sbse.experiment;
//  NSGAIIStudy.java
//
//  Authors:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 
import jmetal.core.Algorithm;
import jmetal.experiments.Experiment;
import jmetal.experiments.Settings;
import jmetal.experiments.util.Friedman;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.qualityIndicator.Epsilon;
import jmetal.qualityIndicator.Hypervolume;
import jmetal.qualityIndicator.InvertedGenerationalDistance;
import jmetal.qualityIndicator.Spread;
import jmetal.qualityIndicator.util.MetricsUtil;
import jmetal.util.JMException;
import jmetal.util.NonDominatedSolutionList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.birzeit.jmse.sbse.FileUtils;
import edu.birzeit.jmse.sbse.MoHrResources;
import edu.birzeit.jmse.sbse.reporting.RadarChartGenerator;

/**
 * Class implementing an example of experiment using NSGA-II as base algorithm.
 * The experiment consisting in studying the effect of the crossover probability
 * in NSGA-II.
 */
public class HrResourcesStudy extends Experiment {

	private static int stopTime = -1;
	private static int runs = 10;

	/**
	 * Configures the algorithms in each independent run
	 * 
	 * @param problemName
	 *            The problem to solve
	 * @param problemIndex
	 * @param algorithm
	 *            Array containing the algorithms to run
	 * @throws Exception
	 */
	public synchronized void algorithmSettings(String problemName, int problemIndex, Algorithm[] algorithm)
			throws Exception {
		try {
			int numberOfAlgorithms = algorithmNameList_.length;

			HashMap[] parameters = new HashMap[numberOfAlgorithms];

			for (int i = 0; i < numberOfAlgorithms; i++) {
				parameters[i] = new HashMap();
			} // for

			if (!paretoFrontFile_[problemIndex].equals("")) {
				for (int i = 0; i < numberOfAlgorithms; i++)
					parameters[i].put("paretoFrontFile_", paretoFrontFile_[problemIndex]);
			} // if

			for (int i = 0; i < numberOfAlgorithms; i++) {
				parameters[i].put("crossoverProbability_", 0.9);
			}
			// parameters[1].put("crossoverProbability_", 0.9);
			// parameters[2].put("crossoverProbability_", 0.8);

			if ((!paretoFrontFile_[problemIndex].equals("")) || (paretoFrontFile_[problemIndex] == null)) {
				for (int i = 0; i < numberOfAlgorithms; i++)
					parameters[i].put("paretoFrontFile_", paretoFrontFile_[problemIndex]);
			} // if

			HrNSGAII_Settings nsgaiiSettings = new HrNSGAII_Settings(problemName);
			nsgaiiSettings.stopTimeInSec_ = this.stopTime;
			algorithm[0] = nsgaiiSettings.configure(parameters[0]);

			if (numberOfAlgorithms > 1) {
				algorithm[1] = new HrIBEA_Settings(problemName).configure(parameters[1]);

				algorithm[2] = new HrMOCell_Settings(problemName).configure(parameters[2]);
			}

		} catch (IllegalArgumentException ex) {
			Logger.getLogger(HrResourcesStudy.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(HrResourcesStudy.class.getName()).log(Level.SEVERE, null, ex);
		} catch (JMException ex) {
			Logger.getLogger(HrResourcesStudy.class.getName()).log(Level.SEVERE, null, ex);
		}
	} // algorithmSettings

	public static void main(String[] args) throws JMException, IOException {

		// double hv = generateHVIndicators( "/Users/EliasK/Google
		// Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/StudyOutput/exp_bugs_150/referenceFronts/exp_bugs.rf",
		// "/Users/EliasK/Google
		// Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/ExperimentResults/Dominated/FUN_ALL2")
		// ;
		long[] period = { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 90, 100, 120, 150, 180, 210, 240, 300 };
		// System.out.println("hv = "+ hv);
		// int times[] = { 30, 60 , 90 , 120 , 180, 300, 450, 600 };
		// int times[] = {5, 10, 15,20, 30, 45, 60, 90 , 120 , 150, 180, 300, 450};

		//long[] period = { 5, 10};
		int times[] = { 500 };
		runs = 1;
		// int times[] = { 30};
		// runs = 1;
		String hvString = "";
		for (int slectedStopTime : times) {
			
			
			
//  
//			NSGAII.problemName = "JDTMilestoneM1";
//			double[][] front1 = { { -100, -100, -50000, 20000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front1;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//
//			NSGAII.problemName = "JDTMilestoneM2";
//			double[][] front2 = { { -100, -100, -500000, 100000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front2;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//
//			NSGAII.problemName = "JDTMilestoneM3";
//			double[][] front3 = { { -100, -100, -500000, 20000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front3;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//
//			NSGAII.problemName = "JDTMilestoneM4";
//			double[][] front4 = { { -180, -180, -900000, 50000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front4;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			
//
//			NSGAII.problemName = "JDTMilestoneM5";
//			double[][] front5 = { { -180, -180, -900000, 50000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front5;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//
//			NSGAII.problemName = "JDTMilestoneM6";
//			double[][] front6 = { { -180, -180, -900000, 50000  }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front6;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//
//			NSGAII.problemName = "JDTMilestone 3.1";
//			double[][] front = { { -100, -100, -500000, 500000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//
//			NSGAII.problemName = "PlatformMilestone3.0";
//			double[][] front7 = { { -100, -100, -5000, 800000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front7;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//
//			NSGAII.problemName = "PlatformMilestone3.1";
//			double[][] front8 = { {  -100, -100, -5000, 800000  }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front8;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM2";
//			double[][] front99 = { { -100, -15, -500000, 100000 }, { 0, 0, 0, 0 } };
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			NSGAII.trueFront = front99;
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			///////////////////
//			NSGAII.problemName = "PlatformMilestoneM3";
//			double[][] front10 = { { -100, -100, -500000, 500000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front10;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM4";
//			double[][] front11 = { {  -100, -100, -500000, 500000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front11;
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM5";
//			double[][] front15 = { { -50, -50, -500000, 500000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front15;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM6";
//			double[][] front16 = { { -50, -50, -500000, 500000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front16;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM7";
//			double[][] front17 = { { -50, -50, -900000, 900000}, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front17;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM8";
//			double[][] front18 = { { -50, -50, -900000, 900000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front18;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
//			
//			NSGAII.problemName = "PlatformMilestoneM9";
//			double[][] front19 = { { -50, -50, -900000, 900000 }, { 0, 0, 0, 0 } };
//			NSGAII.trueFront = front1;
//			hvString = "";
//			NSGAII.periodBase = new long[period.length];
//			for (int i=0; i<period.length; i++) {
//				NSGAII.periodBase[i] = period[i];
//				hvString += period[i] + "\t";
//			}
//			hvString+="\n";
//			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
//			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
//			
//			
			
			NSGAII.problemName = "rks_detailedBugsWithEffort";
			double[][] front20 = { { -20, -20, -3000, 30000 }, { 0, 0, 0, 0 } };
			NSGAII.trueFront = front20;
			hvString = "";
			NSGAII.periodBase = new long[period.length];
			for (int i=0; i<period.length; i++) {
				NSGAII.periodBase[i] = period[i];
				hvString += period[i] + "\t";
			}
			hvString+="\n";
			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
			
			
			
			
			NSGAII.problemName = "xcis_reducedBugsSkills";
			double[][] front21 = { { -20, -20, -30000, 90000 }, { 0, 0, 0, 0 } };
			NSGAII.trueFront = front21;
			hvString = "";
			NSGAII.periodBase = new long[period.length];
			for (int i=0; i<period.length; i++) {
				NSGAII.periodBase[i] = period[i];
				hvString += period[i] + "\t";
			}
			hvString+="\n";
			FileUtils.appendFile(hvString, "HVresults_" + NSGAII.problemName);
			runAProblem(NSGAII.problemName, "JDTDeveloperWithWage", slectedStopTime);
			
			
			
			//
			// NSGAII.hvFileCount = 2 ;
			// double[][] front2 = {{-60, -40, -12000, 330000},{0,0,0,0}} ;
			// NSGAII.trueFront = front2;
			// runAProblem("rks_detailedBugsWithEffort", "JDTDeveloperWithWage",
			// slectedStopTime);
			//
			// NSGAII.hvFileCount = 3 ;
			// double[][] front3 = {{-50, -20, -70000, 400000},{0,0,0,0}} ;
			// NSGAII.trueFront = front3;
			// runAProblem("xcisco_reducedBugsSkills", "JDTDeveloperWithWage",
			// slectedStopTime);
			//
			//
			// runAProblem("exp_bugs", "exp_d;evelopers", slectedStopTime);
			/*
			 * String fun = ""; String var = "";
			 * 
			 * for (int i = 0; i < MoHrResources.objectives.length; i++) { if
			 * (MoHrResources.solutions[i].equals("")) { fun += MoHrResources.funs[i] +
			 * "\n"; var +=
			 * "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"; }
			 * int shortestIdx = 0; double shortestDist =
			 * euclideanDistance(MoHrResources.objectives[i][0],
			 * Double.parseDouble(MoHrResources.funs[0].split(" ")[0]),
			 * MoHrResources.objectives[i][1],
			 * Double.parseDouble(MoHrResources.funs[0].split(" ")[1]),
			 * MoHrResources.objectives[i][2],
			 * Double.parseDouble(MoHrResources.funs[0].split(" ")[2]),
			 * MoHrResources.objectives[i][3],
			 * Double.parseDouble(MoHrResources.funs[0].split(" ")[3]));
			 * 
			 * for (int j = 0; j < MoHrResources.solutions.length; j++) { double dist =
			 * euclideanDistance(MoHrResources.objectives[i][0],
			 * Double.parseDouble(MoHrResources.funs[j].split(" ")[0]),
			 * MoHrResources.objectives[i][1],
			 * Double.parseDouble(MoHrResources.funs[j].split(" ")[1]),
			 * MoHrResources.objectives[i][2],
			 * Double.parseDouble(MoHrResources.funs[j].split(" ")[2]),
			 * MoHrResources.objectives[i][3],
			 * Double.parseDouble(MoHrResources.funs[j].split(" ")[3])); if (dist<
			 * shortestDist) { shortestDist = dist; shortestIdx = j }
			 * FileUtils.writeStringToFile(MoHrResources.solutions[i],
			 * "/Users/EliasK/Downloads/Funs" + i + ".txt");
			 * FileUtils.writeStringToFile(MoHrResources.vars[i],
			 * "/Users/EliasK/Downloads/Vars" + i + ".txt"); }
			 * System.out.println("\n\n\n number of solutions = " + MoHrResources.sCount);
			 * 
			 * }
			 */
		}

	}

	public static void mainxx(String[] args) throws JMException, IOException {
		stopTime = -1;
		HrResourcesStudy exp = new HrResourcesStudy(); // exp = experiment

		exp.experimentName_ = "HrResourcesStudy";
		exp.algorithmNameList_ = new String[] { "NSGAII", "IBEA", "MOCell" };

		exp.problemList_ = new String[] { "xcis_reducedBugsSkills", "rks_detailedBugsWithEffort", "JDTMilestoneM6",
				"PlatformMilestone3.0", "PlatformMilestone3.1", "PlatformMilestoneM2", "PlatformMilestoneM3",
				"PlatformMilestoneM4", "PlatformMilestoneM5", "PlatformMilestoneM6", "PlatformMilestoneM7",
				"PlatformMilestoneM8", "PlatformMilestoneM9" };

		exp.paretoFrontFile_ = new String[] { "xcis_reducedBugsSkills.pf", "rks_detailedBugsWithEffort.pf",
				"JDTMilestoneM6.pf", "PlatformMilestone3.0.pf", "PlatformMilestone3.1.pf", "PlatformMilestoneM2.pf",
				"PlatformMilestoneM3.pf", "PlatformMilestoneM4.pf", "PlatformMilestoneM5.pf", "PlatformMilestoneM6.pf",
				"PlatformMilestoneM7.pf", "PlatformMilestoneM8.pf", "PlatformMilestoneM9.pf" };

		// exp.problemList_ = new String[] { "JDTMilestone 3.1",
		// "JDTMilestone3.1.1", "JDTMilestone3.1.2" };

		// exp.paretoFrontFile_ = new String[] { "JDTMilestone 3.1.pf",
		// "JDTMilestone3.1.1.pf", "JDTMilestone3.1.2.pf" };

		exp.indicatorList_ = new String[] { "HV"/* , "SPREAD", "IGD", "EPSILON" */ };

		int numberOfAlgorithms = exp.algorithmNameList_.length;

		exp.experimentBaseDirectory_ = "StudyOutput/" + exp.experimentName_;
		exp.paretoFrontDirectory_ = "StudyOutput/paretoFronts";

		exp.algorithmSettings_ = new Settings[numberOfAlgorithms];

		exp.independentRuns_ = 30;

		exp.initExperiment();

		// Run the experiments
		int numberOfThreads = 1;
		exp.runExperiment(numberOfThreads);

		exp.generateQualityIndicators();

		// Generate latex tables (comment this sentence is not desired)
		exp.generateLatexTables();

		// Configure the R scripts to be generated int rows; int columns;
		String prefix;
		String[] problems;

		int rows;
		int columns;

		rows = 2;
		columns = 3;
		prefix = new String("Problems");
		problems = new String[] { "xcis_reducedBugsSkills", "rks_detailedBugsWithEffort", "JDTMilestoneM6",
				"PlatformMilestone3.0", "PlatformMilestone3.1", "PlatformMilestoneM2", "PlatformMilestoneM3",
				"PlatformMilestoneM4", "PlatformMilestoneM5", "PlatformMilestoneM6", "PlatformMilestoneM7",
				"PlatformMilestoneM8", "PlatformMilestoneM9" };

		boolean notch;
		exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true, exp);
		exp.generateRWilcoxonScripts(problems, prefix, exp);

		// Applying Friedman
		Friedman test = new Friedman(exp);
		// test.executeTest("EPSILON");
		test.executeTest("HV");
		// test.executeTest("SPREAD");

	} // main

	public static void runAProblem(String problemName, String developersFile, int stopTimeinSec)
			throws JMException, IOException {
		stopTime = stopTimeinSec;
		HrResourcesStudy exp = new HrResourcesStudy(); // exp = experiment

		exp.experimentName_ = problemName + "_" + stopTimeinSec;
		exp.algorithmNameList_ = new String[] { "NSGAII" };// , "IBEA", "MOCell"
															// };

		exp.problemList_ = new String[] { problemName };// , "JDTMilestoneM3" ,
														// "JDTMilestoneM4",
														// "JDTMilestoneM5" };

		exp.paretoFrontFile_ = new String[] { problemName + ".pf" };

		// exp.problemList_ = new String[] { "JDTMilestone 3.1",
		// "JDTMilestone3.1.1", "JDTMilestone3.1.2" };

		// exp.paretoFrontFile_ = new String[] { "JDTMilestone 3.1.pf",
		// "JDTMilestone3.1.1.pf", "JDTMilestone3.1.2.pf" };

		exp.indicatorList_ = new String[] { "HV"/* , "SPREAD", "IGD", "EPSILON" */ };

		int numberOfAlgorithms = exp.algorithmNameList_.length;

		exp.experimentBaseDirectory_ = "StudyOutput/" + exp.experimentName_;
		exp.paretoFrontDirectory_ = "StudyOutput/paretoFronts";

		exp.algorithmSettings_ = new Settings[numberOfAlgorithms];

		exp.independentRuns_ = runs;

		exp.initExperiment();

		// Run the experiments
		int numberOfThreads = 1;
		exp.runExperiment(numberOfThreads);

		exp.generateQualityIndicators();

		// Generate latex tables (comment this sentence is not desired)
		exp.generateLatexTables();

		// Configure the R scripts to be generated int rows; int columns;
		String prefix;
		String[] problems;

		int rows;
		int columns;

		rows = 2;
		columns = 3;
		prefix = new String("Problems");
		problems = new String[] { problemName };

		boolean notch;
		exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true, exp);
		exp.generateRWilcoxonScripts(problems, prefix, exp);

		// Applying Friedman
		Friedman test = new Friedman(exp);
		// test.executeTest("EPSILON");
		test.executeTest("HV");
		// test.executeTest("SPREAD");
		RadarChartGenerator
				.addSolutionChart("/Users/EliasK/Google Drive/Thesis/Dev/bugplanning/workspace/HRplan4Bugs/StudyOutput/"
						+ problemName + "_" + stopTimeinSec + "/data/NSGAII/" + problemName + "/");

	} // main

	public void generateReferenceFronts(int problemIndex) {

		File rfDirectory;
		String referenceFrontDirectory = experimentBaseDirectory_ + "/referenceFronts";

		rfDirectory = new File(referenceFrontDirectory);

		if (!rfDirectory.exists()) { // Si no existe el directorio
			boolean result = new File(referenceFrontDirectory).mkdirs(); // Lo creamos
			System.out.println("Creating " + referenceFrontDirectory);
		}

		frontPath_[problemIndex] = referenceFrontDirectory + "/" + problemList_[problemIndex] + ".rf";

		MetricsUtil metricsUtils = new MetricsUtil();
		NonDominatedSolutionList solutionSet = new NonDominatedSolutionList();
		for (String anAlgorithmNameList_ : algorithmNameList_) {

			String problemDirectory = experimentBaseDirectory_ + "/data/" + anAlgorithmNameList_ + "/"
					+ problemList_[problemIndex];

			for (int numRun = 0; numRun < independentRuns_; numRun++) {

				String outputParetoFrontFilePath;
				outputParetoFrontFilePath = problemDirectory + "/FUN." + numRun;
				String solutionFrontFile = outputParetoFrontFilePath;

				metricsUtils.readNonDominatedSolutionSet(solutionFrontFile, solutionSet);
			} // for
		} // for
		String pf = "";
		pf += "-60 -40 12000	330000\n0 0 0 0";
		FileUtils.writeStringToFile(pf, frontPath_[problemIndex]);
		System.out.println("PFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPF");
		// solutionSet.printObjectivesToFile(frontPath_[problemIndex]);
	} // generateReferenceFronts

	static double euclideanDistance(double o1, double p1, double o2, double p2, double o3, double p3, double o4,
			double p4) {

		return Math.sqrt(Math.pow((o1 - p1), 2.0) + Math.pow((o2 - p2), 2.0) + Math.pow((o3 - p3), 2.0)
				+ Math.pow((o4 - p4), 2.0));
	}

	public static double generateHVIndicators(String paretoFrontPath, String solutionFrontFile) {

		double[][] trueFront = new Hypervolume().utils_.readFront(paretoFrontPath);
		Hypervolume indicators = new Hypervolume();
		double[][] solutionFront = indicators.utils_.readFront(solutionFrontFile);
		// double[][] trueFront =
		// indicators.utils_.readFront(paretoFrontPath);
		return indicators.hypervolume(solutionFront, trueFront, trueFront[0].length);

	}

} // NSGAIIStudy
