package edu.birzeit.jmse.sbse.reader;

import java.util.Date;

import edu.birzeit.jmse.sbse.FileUtils;
import edu.birzeit.jmse.sbse.experiment.CommonSettings;

public class DatasetReader {
	public static final int ID = 0, JDT = 1, JFACE = 2, CORE = 3, SWT = 4, UI = 5, DEBUG = 6, LTK = 7, TEXT = 8,
			JDI = 9, SEARCH = 10, COMPARE = 11, OTHER = 12, JDTWAGE = 13;

	public static final int PROF1 = 1, PROF2 = 2, PROF3 = 3, PROF4 = 4, PROF5 = 5, PROF6 = 6, PROF7 = 7, PROF8 = 8,
			PROF9 = 9, PROF10 = 10, PROF11 = 11, PROF12 = 12, PLATEFORMWAGE = 13;

	public static final int EFFORT = 2;

	public static int DEV_JDT_START_SKILL_IDX = 1;
	public static int DEV_JDT_END_SKILL_IDX = 12;
	public static int BUG_JDT_START_SKILL_IDX = 3;
	public static int BUG_JDT_END_SKILL_IDX = 14;

	public static int DEV_PLATFORM_START_SKILL_IDX = 1;
	public static int DEV_PLATFORM_END_SKILL_IDX = 12;
	public static int BUG_PLATFORM_START_SKILL_IDX = 3;
	public static int BUG_PLATFORM_END_SKILL_IDX = 14;

	static public int SEVERITY_CSV_IDX = 4, PRIORITY_CSV_IDX = 5, CREATE_TIME_IDX = 6;

	static public int SEVERITY = 15, PRIORITY = 16, CREATE_DATE = 17, CUSTOMER_BUG = 18;

	public static double[][] readDataFile(String filepath) {

		String[] devData = FileUtils.readFileToString(filepath).split("\n");
		String[] titles = devData[0].split("\t");

		double[][] result = new double[devData.length - 1][titles.length]; // i-1
																			// as
																			// we
																			// ignore
																			// the
																			// first
																			// line
																			// (titles)

		for (int i = 1; i < devData.length; i++) {
			String[] devParams = devData[i].split("\t");
			for (int j = 0; j < devParams.length; j++) {
				result[i - 1][j] = Double.parseDouble(devParams[j]);
				// System.out.print(result[i - 1][j] + " ");
			}
			// System.out.println(" ");
		}

		return result;

	}

	public static String[][] readCsvFile(String filepath) {

		String[] devData = FileUtils.readFileToString(filepath).split("\n");
		String[] titles = devData[0].split(",");

		String[][] result = new String[devData.length - 1][titles.length]; // i-1
																			// as
																			// we
																			// ignore
																			// the
																			// first
																			// line
																			// (titles)

		for (int i = 1; i < devData.length; i++) {
			String[] devParams = devData[i].split(",");
			for (int j = 0; j < devParams.length; j++) {
				result[i - 1][j] = devParams[j];
				// System.out.print(result[i - 1][j] + " ");
			}
			// System.out.println(" ");
		}

		return result;

	}

	public static double[][] fillBugProperties(double[][] basicBugs) throws Exception {
		double[][] filledBugs = new double[basicBugs.length][basicBugs[0].length + 4];
		for (int i = 0; i < basicBugs.length; i++) {
			for (int j = 0; j < basicBugs[0].length; j++) {
				filledBugs[i][j] = basicBugs[i][j];
			}
		}
		// System.out.println("basicBugs width = " + basicBugs[0].length);
		// System.out.println("filledBugs width = " + filledBugs[0].length);

		String datasetDir = CommonSettings.PROJ_DIR + "/dataset";
		// String datasetDir ="/home/ec2-user/hropt/dataset" ;
		// get bugs properties from csvs
		String[][] csvData1 = readCsvFile(datasetDir + "/JDTMilestone3.1.1e.csv");
		String[][] csvData2 = readCsvFile(datasetDir + "/PlatformMilestone3.1e.csv");
		String[][] csvData3 = readCsvFile(datasetDir + "/JDTMilestone3.1.2e.csv");
		String[][] csvData4 = readCsvFile(datasetDir + "/PlatformMilestoneM2e.csv");
		String[][] csvData5 = readCsvFile(datasetDir + "/JDTMilestone3.1e.csv");
		String[][] csvData6 = readCsvFile(datasetDir + "/PlatformMilestoneM3e.csv");
		String[][] csvData7 = readCsvFile(datasetDir + "/JDTMilestoneM1e.csv");
		String[][] csvData8 = readCsvFile(datasetDir + "/PlatformMilestoneM4e.csv");
		String[][] csvData9 = readCsvFile(datasetDir + "/JDTMilestoneM2e.csv");
		String[][] csvData10 = readCsvFile(datasetDir + "/PlatformMilestoneM5e.csv");
		String[][] csvData11 = readCsvFile(datasetDir + "/JDTMilestoneM3e.csv");
		String[][] csvData12 = readCsvFile(datasetDir + "/PlatformMilestoneM6e.csv");
		String[][] csvData13 = readCsvFile(datasetDir + "/JDTMilestoneM4e.csv");
		String[][] csvData14 = readCsvFile(datasetDir + "/PlatformMilestoneM7e.csv");
		String[][] csvData15 = readCsvFile(datasetDir + "/JDTMilestoneM5e.csv");
		String[][] csvData16 = readCsvFile(datasetDir + "/PlatformMilestoneM8e.csv");
		String[][] csvData17 = readCsvFile(datasetDir + "/JDTMilestoneM6e.csv");
		String[][] csvData18 = readCsvFile(datasetDir + "/PlatformMilestoneM9e.csv");
		String[][] csvData19 = readCsvFile(datasetDir + "/PlatformMilestone3.0e.csv");

		String[][] mergedData = mergeTwoCsvArrays(csvData1, csvData2);
		mergedData = mergeTwoCsvArrays(mergedData, csvData3);
		mergedData = mergeTwoCsvArrays(mergedData, csvData4);
		mergedData = mergeTwoCsvArrays(mergedData, csvData5);
		mergedData = mergeTwoCsvArrays(mergedData, csvData6);
		mergedData = mergeTwoCsvArrays(mergedData, csvData7);
		mergedData = mergeTwoCsvArrays(mergedData, csvData8);
		mergedData = mergeTwoCsvArrays(mergedData, csvData9);
		mergedData = mergeTwoCsvArrays(mergedData, csvData10);
		mergedData = mergeTwoCsvArrays(mergedData, csvData11);
		mergedData = mergeTwoCsvArrays(mergedData, csvData12);
		mergedData = mergeTwoCsvArrays(mergedData, csvData13);
		mergedData = mergeTwoCsvArrays(mergedData, csvData14);
		mergedData = mergeTwoCsvArrays(mergedData, csvData15);
		mergedData = mergeTwoCsvArrays(mergedData, csvData16);
		mergedData = mergeTwoCsvArrays(mergedData, csvData17);
		mergedData = mergeTwoCsvArrays(mergedData, csvData18);
		mergedData = mergeTwoCsvArrays(mergedData, csvData19);

		// System.out.println("all bugs = " + mergedData.length);
		// loop on all bugs and fill sevirity and priority
		for (int i = 0; i < basicBugs.length; i++) {
			// Severity
			double severity = 0;
			for (int bug = 0; bug < mergedData.length - 1; bug++) {
				// System.out.println(bug + ":" + mergedData[bug][0] + " , " + basicBugs[i][0]
				// );
				if (mergedData[bug][0] == null) {
					continue;
				}
				double mergedBugId = 0;

				try {
					mergedBugId = Double.parseDouble(mergedData[bug][0].trim());
				} catch (Exception e) {
					continue;
				}

				// System.out.println(bug + ":" + mergedData[bug][0] );
				if (mergedBugId == basicBugs[i][0]) {
					if (mergedData[bug].length < 5) {
						// System.out.println("Empty row ......");
					} else if (mergedData[bug][SEVERITY_CSV_IDX].toLowerCase().equals("enhancement")) {
						severity = 1;
						break;
					} else if (mergedData[bug][SEVERITY_CSV_IDX].toLowerCase().equals("normal")) {
						severity = 2;
						break;
					} else if (mergedData[bug][SEVERITY_CSV_IDX].toLowerCase().equals("major")) {
						severity = 3;
						break;
					} else if (mergedData[bug][SEVERITY_CSV_IDX].toLowerCase().equals("critical")) {
						severity = 4;
						break;
					} else if (mergedData[bug][SEVERITY_CSV_IDX].toLowerCase().equals("blocker")) {
						severity = 5;
						break;
					}
				}
			}
			filledBugs[i][filledBugs[0].length - 4] = severity;

			// Priority
			double priority = 0;
			for (int bug = 0; bug < mergedData.length - 1; bug++) {
				if (mergedData[bug][0] == null) {
					continue;
				}
				double mergedBugId = 0;

				try {
					mergedBugId = Double.parseDouble(mergedData[bug][0].trim());
				} catch (Exception e) {
					continue;
				}
				if (mergedBugId == basicBugs[i][0]) {

					if (mergedData[bug][PRIORITY_CSV_IDX].toLowerCase().equals("p1")) {
						priority = 1;
						break;
					} else if (mergedData[bug][PRIORITY_CSV_IDX].toLowerCase().equals("p2")) {
						priority = 2;
						break;
					} else if (mergedData[bug][PRIORITY_CSV_IDX].toLowerCase().equals("p3")) {
						priority = 3;
						break;
					} else if (mergedData[bug][PRIORITY_CSV_IDX].toLowerCase().equals("p4")) {
						priority = 4;
						break;
					} else if (mergedData[bug][PRIORITY_CSV_IDX].toLowerCase().equals("p5")) {
						priority = 5;
						break;
					}
				}
			}

			filledBugs[i][filledBugs[0].length - 3] = priority;

			// Throw an exception if priority or severity is not set
			if (severity == 0 || priority == 0) {
				// System.out.println("priority or severity are not set for bug "+
				// filledBugs[i][0]);
			} else {
				// System.out.println("....................................... "+
				// filledBugs[i][0]);
			}
			// Create time
			String creatTimeStr = "";

			for (int bug = 0; bug < mergedData.length - 1; bug++) {
				if (mergedData[bug][0] == null) {
					continue;
				}
				double mergedBugId = 0;

				try {
					mergedBugId = Double.parseDouble(mergedData[bug][0].trim());
				} catch (Exception e) {
					continue;
				}
				if (mergedBugId == basicBugs[i][0]) {

					creatTimeStr = mergedData[bug][CREATE_TIME_IDX].trim();
				}
			}

			if (creatTimeStr != null && creatTimeStr != "") {
				try {
					Date date = new Date(creatTimeStr);
					Date now = new Date("12/31/2019");
					filledBugs[i][filledBugs[0].length - 2] = (now.getTime() - date.getTime()) / (24000 * 60 * 60);
				} catch (IllegalArgumentException e) {
					filledBugs[i][filledBugs[0].length - 2] = 0;
				}
			} else {
				filledBugs[i][filledBugs[0].length - 2] = 0;
				// System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nMISSING
				// CREATION DATE
				// \n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}

			// Throw an exception if priority or severity is not set
			if (severity == 0 || priority == 0) {
				// System.out.println("priority or severity are not set for bug "+
				// filledBugs[i][0]);
			} else {
				// System.out.println("....................................... "+
				// filledBugs[i][0]);
			}

			////////

		}

		// printBugsDetails(filledBugs);

		return filledBugs;

	}

	static public void printBugsDetails(double[][] bugs) {
		System.out.println("\n");
		for (int i = 0; i < bugs.length; i++) {
			for (int j = 0; j < bugs[0].length; j++) {
				System.out.print(bugs[i][j] + " , ");
			}
			System.out.println("\n");
		}
	}

	public static void main(String[] args) {

		// double[][] devData =
		// readDataFile("/Users/EliasK/GoogleDrive/BZUmaster/Thesis/Dev/repos/HrOpt/HrResourcesOpt/dataset/JDTMilestone
		// 3.1.txt");

		// String [][] csvData1 =
		// readCsvFile("/Users/EliasK/GoogleDrive/BZUmaster/Thesis/Dev/repos/HrOpt/HrResourcesOpt/dataset/JDTMilestoneM2e.csv");
		// String [][] csvData2 =
		// readCsvFile("/Users/EliasK/GoogleDrive/BZUmaster/Thesis/Dev/repos/HrOpt/HrResourcesOpt/dataset/JDTMilestoneM3e.csv");
		String datasetDir = "/Users/EliasK/Work/Dev/thesis/HrResourcesOpt/dataset";
		String[][] csvData1 = readCsvFile(datasetDir + "/JDTMilestone3.1.1e.csv");
		String[][] csvData2 = readCsvFile(datasetDir + "/PlatformMilestone3.1e.csv");
		String[][] csvData3 = readCsvFile(datasetDir + "/JDTMilestone3.1.2e.csv");
		String[][] csvData4 = readCsvFile(datasetDir + "/PlatformMilestoneM2e.csv");
		String[][] csvData5 = readCsvFile(datasetDir + "/JDTMilestone3.1e.csv");
		String[][] csvData6 = readCsvFile(datasetDir + "/PlatformMilestoneM3e.csv");
		String[][] csvData7 = readCsvFile(datasetDir + "/JDTMilestoneM1e.csv");
		String[][] csvData8 = readCsvFile(datasetDir + "/PlatformMilestoneM4e.csv");
		String[][] csvData9 = readCsvFile(datasetDir + "/JDTMilestoneM2e.csv");
		String[][] csvData10 = readCsvFile(datasetDir + "/PlatformMilestoneM5e.csv");
		String[][] csvData11 = readCsvFile(datasetDir + "/JDTMilestoneM3e.csv");
		String[][] csvData12 = readCsvFile(datasetDir + "/PlatformMilestoneM6e.csv");
		String[][] csvData13 = readCsvFile(datasetDir + "/JDTMilestoneM4e.csv");
		String[][] csvData14 = readCsvFile(datasetDir + "/PlatformMilestoneM7e.csv");
		String[][] csvData15 = readCsvFile(datasetDir + "/JDTMilestoneM5e.csv");
		String[][] csvData16 = readCsvFile(datasetDir + "/PlatformMilestoneM8e.csv");
		String[][] csvData17 = readCsvFile(datasetDir + "/JDTMilestoneM6e.csv");
		String[][] csvData18 = readCsvFile(datasetDir + "/PlatformMilestoneM9e.csv");
		String[][] csvData19 = readCsvFile(datasetDir + "/PlatformMilestone3.0e.csv");

		String[][] mergedData = mergeTwoCsvArrays(csvData1, csvData2);
		mergedData = mergeTwoCsvArrays(mergedData, csvData3);
		mergedData = mergeTwoCsvArrays(mergedData, csvData4);
		mergedData = mergeTwoCsvArrays(mergedData, csvData5);
		mergedData = mergeTwoCsvArrays(mergedData, csvData6);
		mergedData = mergeTwoCsvArrays(mergedData, csvData7);
		mergedData = mergeTwoCsvArrays(mergedData, csvData8);
		mergedData = mergeTwoCsvArrays(mergedData, csvData9);
		mergedData = mergeTwoCsvArrays(mergedData, csvData10);
		mergedData = mergeTwoCsvArrays(mergedData, csvData11);
		mergedData = mergeTwoCsvArrays(mergedData, csvData12);
		mergedData = mergeTwoCsvArrays(mergedData, csvData13);
		mergedData = mergeTwoCsvArrays(mergedData, csvData14);
		mergedData = mergeTwoCsvArrays(mergedData, csvData15);
		mergedData = mergeTwoCsvArrays(mergedData, csvData16);
		mergedData = mergeTwoCsvArrays(mergedData, csvData17);
		mergedData = mergeTwoCsvArrays(mergedData, csvData18);
		mergedData = mergeTwoCsvArrays(mergedData, csvData19);

		// System.out.println(mergedData.length + " " + mergedData[0].length);

	}

	static String[][] mergeTwoCsvArrays(String[][] csvData1, String[][] csvData2) {
		String[][] result = new String[csvData1.length + csvData2.length][csvData1[0].length];
		int count = 0;
		for (int i = 0; i < csvData1.length; i++) {
			for (int j = 0; j < csvData1[0].length; j++) {
				result[count][j] = csvData1[i][j];
			}
			count++;
		}
		for (int i = 0; i < csvData2.length; i++) {
			for (int j = 0; j < csvData2[0].length; j++) {
				result[count][j] = csvData2[i][j];
			}
			count++;
		}
		return result;

	}

	public static double[][] loadDirectBugProperties(String bugsFilePath) throws Exception {
		int currentLine = 0, currtentColumn = 0;
		double[][] basicBugs = null ;
		try {
			String content = FileUtils.readFileToString(bugsFilePath);
			String[] lines = content.split("\n");
			int numberOflines = lines.length; // -1 to exclude header
			int numberofColumns = lines[1].split("\t").length;
			 basicBugs = new double[numberOflines][numberofColumns];
			for (int line = 1; line < numberOflines; line++) {
				currentLine= line;
				String[] lineFields = lines[line].split("\t");
				for (int column = 0; column < numberofColumns; column++) {
					currtentColumn = column ;
					// if (column == 0) {
					// lineFields[0] = lineFields[0].substring(4, lineFields[0].length()-1);
					// }
					basicBugs[line - 1][column] = Double.parseDouble(lineFields[column]);
				}
			}
			CUSTOMER_BUG = numberofColumns - 1;
			CREATE_DATE = numberofColumns - 2;
			PRIORITY = numberofColumns - 3;
			SEVERITY = numberofColumns - 4;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("line = " + currentLine + " column = " + currtentColumn);
		}

		return basicBugs;

	}

}
