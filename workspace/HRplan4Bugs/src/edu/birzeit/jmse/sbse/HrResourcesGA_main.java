package edu.birzeit.jmse.sbse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.singleObjective.geneticAlgorithm.gGA;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;

public class HrResourcesGA_main {

	public static void main(String[] args)
			throws Exception {
		Problem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		HashMap parameters; // Operator parameters
		
		String problemName = "resources/bugs100.bugs";
		String employeeFilePath = "resources/Employee4.emp";
		int bits = 500;
		
		problem = new SoHrResources("Binary", problemName, employeeFilePath);

		algorithm = new gGA(problem) ;
		
		
		
		// Algorithm params
		algorithm.setInputParameter("populationSize", 100);
		algorithm.setInputParameter("maxEvaluations", 1000000);

	    // Mutation and Crossover for Binary codification 
	    parameters = new HashMap() ;
	    parameters.put("probability", 0.9) ;
	    crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);                   

	    parameters = new HashMap() ;
	    parameters.put("probability", 1.0/bits) ;
	    mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);                    
	    
	    /* Selection Operator */
	    parameters = null ;
	    selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters) ;                            
	    
	    /* Add the operators to the algorithm*/
	    algorithm.addOperator("crossover",crossover);
	    algorithm.addOperator("mutation",mutation);
	    algorithm.addOperator("selection",selection);

		/* Execute the Algorithm */
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;
		System.out.println("Total time of execution: " + estimatedTime);

		/* Log messages */
		System.out.println("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN_Sing_GA");
		System.out.println("Variables values have been writen to file VAR");
		population.printVariablesToFile("VAR_Sing_GA");
		System.out.println("maxFitness = " + (3- HrResources.maxFitness));
		
		try {
			GanttGenerator.generateGantt((SoHrResources)problem, population);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
