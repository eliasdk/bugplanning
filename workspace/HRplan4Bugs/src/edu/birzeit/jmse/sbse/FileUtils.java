package edu.birzeit.jmse.sbse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

 
public class FileUtils {

	public static void main(String[] args) {
		System.out.println("hello");
		 String content = readFileToString("/Users/elias/Desktop/nabar48sample.txt");

		 content = content.replaceAll("\n",   "\\\\r\\\\n\" + \r\n\"");
		 writeStringToFile(content, "/Users/elias/Desktop/nabar48sample.txt");
	}
	
	public static void writeStringToFile(String str, String filePath) {
		  try {
	            
	            File newTextFile = new File(filePath);

	            FileWriter fw = new FileWriter(newTextFile);
	            fw.write(str);
	            fw.close();

	        } catch (IOException iox) {
	            //do stuff with exception
	            iox.printStackTrace();
	        }
 
	} 
	
	public static String readFileToString (String filePath) {
	try {
		File file = new File(filePath);
		FileReader fileReader = new FileReader(file);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		StringBuffer stringBuffer = new StringBuffer();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			stringBuffer.append(line);
			stringBuffer.append("\n");
		}
		fileReader.close();
		return stringBuffer.toString();
		
	} catch (Exception e) {
		e.printStackTrace();
		return "";
	}
 
	
 }

	public static void appendFile (String str, String filePath) {
		String content = readFileToString ( filePath);
		content+=str;
		writeStringToFile(content, filePath);
	}
}
