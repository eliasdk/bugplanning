package edu.birzeit.jmse.sbse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BugAssignment {

	int bugId=0;
	int sequence=0;
	int developerId=0;
	
	static  List<Integer> uniqueNumbers ;

	public static void setUniqueNumbers(List<Integer> uniqueNumbers) {
		BugAssignment.uniqueNumbers = uniqueNumbers;
	}


	public static Comparator<BugAssignment> BugAssignmentComparator = new Comparator<BugAssignment>() {
	
		 public int compare(BugAssignment thisBug, BugAssignment anotherBug) {
			if(thisBug.sequence >  anotherBug.sequence) {
				return 1;
			}
			else if (thisBug.sequence <  anotherBug.sequence) {
				return -1;
			}
			else {
				if(thisBug.sequence %2 == 0) {
					return thisBug.bugId -  anotherBug.bugId ;
				}
				else {
					return anotherBug.bugId - thisBug.bugId  ;
				}
			}
	 }};
	 public static Comparator<BugAssignment> BugAssignmentGlobalComparator = new Comparator<BugAssignment>() {
			
		 public int compare(BugAssignment thisBug, BugAssignment anotherBug) {
			return uniqueNumbers.get(thisBug.getBugId()).intValue() - uniqueNumbers.get(anotherBug.getBugId()).intValue() ;
	 }};
 
	
	public int getBugId() {
		return bugId;
	}
	public void setBugId(int bugId) {
		this.bugId = bugId;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

    public int getDeveloperId() {
		return developerId;
	}

	public void setDeveloperId(int developerId) {
		this.developerId = developerId;
	}
	 
 
	public String toString() {
		return  "(bugId = " + bugId+ ", sequence = " +  sequence +", developerId = " + developerId + " ) " ;
		
	}
	
}
