//  PMXCrossover.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package edu.birzeit.jmse.jmetal.extension;

import jmetal.core.Solution;
import jmetal.encodings.solutionType.PermutationSolutionType;
import jmetal.encodings.variable.Permutation;
import jmetal.operators.crossover.Crossover;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import edu.birzeit.jmse.jmetal.extension.MixedPermutation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This class allows to apply a PMX crossover operator using two parent
 * solutions.
 * NOTE: the operator is applied to the first encodings.variable of the solutions, and
 * the type of those variables must be VariableType_.Permutation.
 */
public class MixedPMXCrossover extends Crossover {

		
  /**
   * Valid solution types to apply this operator
   */
  private static final List VALID_TYPES = Arrays.asList(MixedPermutationSolutionType.class);

  private Double crossoverProbability_ = null;

  private PMXvVar [] r1 = null;
  private PMXvVar [] r2 = null ;
  /**
   * Constructor
   */
  public MixedPMXCrossover(HashMap<String, Object> parameters) {
    super(parameters);

    if (parameters.get("probability") != null)
      crossoverProbability_ = (Double) parameters.get("probability");
    
    
  } // PMXCrossover

  /**
   * Perform the crossover operation
   *
   * @param probability Crossover probability
   * @param parent1     The first parent
   * @param parent2     The second parent
   * @return An array containig the two offsprings
   * @throws JMException
   */
  public Solution[] doCrossover(double probability,
                                Solution parent1,
                                Solution parent2) {

	int permutationLength;
	permutationLength = ((MixedPermutation) parent1.getDecisionVariables()[0]).getLength();
	
	if(r1 == null ) {
		 r1 =  new PMXvVar [permutationLength];
		 r2  = new PMXvVar [permutationLength];
		
		for (int i = 0; i < permutationLength; i++) {
			 r1[i] =  new PMXvVar();
			 r2[i] =  new PMXvVar();
		}
				
	}
	
    Solution[] offspring = new Solution[2];

    offspring[0] = new Solution(parent1);
    offspring[1] = new Solution(parent2);

   

    

    int parent1Vector[][] = ((MixedPermutation) parent1.getDecisionVariables()[0]).vector_;
    int parent2Vector[][] = ((MixedPermutation) parent2.getDecisionVariables()[0]).vector_;
    int offspring1Vector[][] = ((MixedPermutation) offspring[0].getDecisionVariables()[0]).vector_;
    int offspring2Vector[][] = ((MixedPermutation) offspring[1].getDecisionVariables()[0]).vector_;

    //TODO: elias remove this
    if(!isValidMixedPermutation(parent1Vector,permutationLength)) {
    	System.out.println("Invalid parent2Vector");
    }
    if(!isValidMixedPermutation(parent1Vector,permutationLength)) {
    	System.out.println("Invalid parent2Vector");
    }
    /////////
    
    if (PseudoRandom.randDouble() < probability) {
      int cuttingPoint1;
      int cuttingPoint2;

      //      STEP 1: Get two cutting points
      cuttingPoint1 = PseudoRandom.randInt(0, permutationLength - 1);
      cuttingPoint2 = PseudoRandom.randInt(0, permutationLength - 1);
      while (cuttingPoint2 == cuttingPoint1)
        cuttingPoint2 = PseudoRandom.randInt(0, permutationLength - 1);

      if (cuttingPoint1 > cuttingPoint2) {
        int swap;
        swap = cuttingPoint1;
        cuttingPoint1 = cuttingPoint2;
        cuttingPoint2 = swap;
      } // if
      //      STEP 2: Get the subchains to interchange
     
     /* PMXvVar replacement1[] = new PMXvVar[permutationLength];
      PMXvVar replacement2[]= new PMXvVar [permutationLength];*/
      PMXvVar replacement1[] = r1;
      PMXvVar replacement2[]= r2;
      
      for (int i = 0; i < permutationLength; i++) {
	    	  if(replacement1[i] == null) {
	    	  	//replacement1[i] = new PMXvVar();
	    	  	//replacement2[i] = new PMXvVar();
	    	  }
    	    replacement1[i].sequence = -1;
    	    replacement2[i].sequence = -1;
      }

      //      STEP 3: Interchange
      for (int i = cuttingPoint1; i <= cuttingPoint2; i++) {
        offspring1Vector[i][0] = parent2Vector[i][0];
        offspring1Vector[i][1] = parent2Vector[i][1];
        offspring2Vector[i][0] = parent1Vector[i][0];
        offspring2Vector[i][1] = parent1Vector[i][1];

        replacement1[parent2Vector[i][1]].developer = parent1Vector[i][0];
        replacement1[parent2Vector[i][1]].sequence = parent1Vector[i][1];
        
        replacement2[parent1Vector[i][1]].developer = parent2Vector[i][0];
        replacement2[parent1Vector[i][1]].sequence = parent2Vector[i][1];
      } // for

      //      STEP 4: Repair offsprings
      for (int i = 0; i < permutationLength; i++) {
        if ((i >= cuttingPoint1) && (i <= cuttingPoint2))
          continue;

        PMXvVar n1 = new PMXvVar();
        n1.developer = parent1Vector[i][0];
        n1.sequence = parent1Vector[i][1];
        
        PMXvVar m1 = replacement1[n1.sequence];

        PMXvVar n2 = new PMXvVar();
        n2.developer = parent2Vector[i][0];
        n2.sequence =   parent2Vector[i][1];
        PMXvVar m2 = replacement2[n2.sequence];

        ///TODO: Elias remove these count
        int count1=0;
        int count2=0;
        while (m1.sequence != -1) {
        	  count1++;
          n1 = m1;
          m1 = replacement1[m1.sequence];
          if(count1==100000) {
        	  	System.out.println("here 1 ");
          }
        } // while
        while (m2.sequence != -1) {
        	  count2++;
          n2 = m2;
          m2 = replacement2[m2.sequence];
          if(count2==100000) {
      	  	System.out.println("here 2");
        }
        } // while
        
        offspring1Vector[i][0] = n1.developer;
        offspring1Vector[i][1] = n1.sequence;
        
        offspring2Vector[i][0] = n2.developer;
        offspring2Vector[i][1] = n2.sequence;
      } // for
    } // if

    //TODO : Elias: remove this
    //Validate permutation
    
    //TODO: elias remove this
    if(!isValidMixedPermutation(parent1Vector,permutationLength)) {
    	System.out.println("Invalid parent2Vector");
    }
    if(!isValidMixedPermutation(parent1Vector,permutationLength)) {
    	System.out.println("Invalid parent2Vector");
    }
    /////////
    
    return offspring;
  } // doCrossover

  /**
   * Executes the operation
   *
   * @param object An object containing an array of two solutions
   * @throws JMException
   */
  public Object execute(Object object) throws JMException {
    Solution[] parents = (Solution[]) object;
    Double crossoverProbability = null;

    if (!(VALID_TYPES.contains(parents[0].getType().getClass()) &&
            VALID_TYPES.contains(parents[1].getType().getClass()))) {

      Configuration.logger_.severe("PMCCrossover.execute: the solutions " +
              "are not of the right type. The type should be 'Permutation', but " +
              parents[0].getType() + " and " +
              parents[1].getType() + " are obtained");
    }

    //crossoverProbability = (Double)parameters_.get("probability");
    crossoverProbability = (Double) getParameter("probability");

    if (parents.length < 2) {
      Configuration.logger_.severe("MixedPMXCrossover.execute: operator needs two " +
              "parents");
      Class cls = java.lang.String.class;
      String name = cls.getName();
      throw new JMException("Exception in " + name + ".execute()");
    }

    Solution[] offspring = doCrossover(crossoverProbability.doubleValue(),
            parents[0],
            parents[1]);

    return offspring;
  } // execute
  
  //TODO: elias remove
  static public boolean isValidMixedPermutation (int[][] data, int len){
	  int [] ref = new int [len]   ;
	  for (int i=0; i<len; i++) {
		  ref[i] = -1;
	  }
	  for (int i=0; i<len; i++) {
	    if(ref[ data[i][1]] != -1 )	{
	    	return false;
	    }
	    ref[data[i][1]] = data[i][1] ;
	  }
	  for (int i=0; i<len; i++) {
		  if(ref[i] == -1) {
			  return false;
		  }
	  }
	  return true;
  }
} // PMXCrossover
