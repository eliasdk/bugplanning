package edu.birzeit.jmse.jmetal.extension;

import jmetal.util.PseudoRandom;

public class CrossoverTest {

	static PMXvVar r1[] = { new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(),
			new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(),
			new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar() };
	static PMXvVar r2[] = { new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(),
			new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar(),
			new PMXvVar(), new PMXvVar(), new PMXvVar(), new PMXvVar() };

	public static void main(String[] args) {

		int permutationLength;

		permutationLength = 9;

		int parent1Vector[][] = { { 2, 6 }, { 1, 1 }, { 9, 7 }, { 0, 7 }, { 3, 4 }, { 1, 8 }, { 8, 2 }, { 0, 5 },
				{ 11, 3 } };
		int parent2Vector[][] = { { 15, 6 }, { 2, 5 }, { 3, 7 }, { 10, 1 }, { 1, 8 }, { 8, 3 }, { 2, 0 }, { 10, 2 },
				{ 7, 4 } };
		int offspring1Vector[][] = { { 2, 6 }, { 1, 1 }, { 9, 7 }, { 0, 7 }, { 3, 4 }, { 1, 8 }, { 8, 2 }, { 0, 5 },
				{ 11, 3 } };
		int offspring2Vector[][] = { { 15, 6 }, { 2, 5 }, { 3, 7 }, { 10, 1 }, { 1, 8 }, { 8, 3 }, { 2, 0 }, { 10, 2 },
				{ 7, 4 } };

		if( !isValidMixedPermutation(parent1Vector, 9) ){
			System.out.println("parent1Vector vector is not valid");
		}
		if( !isValidMixedPermutation(parent2Vector, 9) ){
			System.out.println("parent2Vector vector is not valid");
		}
		if (PseudoRandom.randDouble() < 1) {
			int cuttingPoint1;
			int cuttingPoint2;

			// STEP 1: Get two cutting points
			// cuttingPoint1 = PseudoRandom.randInt(0, permutationLength - 1);
			// cuttingPoint2 = PseudoRandom.randInt(0, permutationLength - 1);

			cuttingPoint1 = 0;
			cuttingPoint2 = 5;

			while (cuttingPoint2 == cuttingPoint1)
				cuttingPoint2 = PseudoRandom.randInt(0, permutationLength - 1);

			if (cuttingPoint1 > cuttingPoint2) {
				int swap;
				swap = cuttingPoint1;
				cuttingPoint1 = cuttingPoint2;
				cuttingPoint2 = swap;
			} // if
				// STEP 2: Get the subchains to interchange

			System.out.println(cuttingPoint1 + " , " + cuttingPoint2);

			/*
			 * PMXvVar replacement1[] = new PMXvVar[permutationLength]; PMXvVar
			 * replacement2[]= new PMXvVar [permutationLength];
			 */
			PMXvVar replacement1[] = r1;
			PMXvVar replacement2[] = r2;

			for (int i = 0; i < permutationLength; i++) {
				if (replacement1[i] == null) {
					// replacement1[i] = new PMXvVar();
					// replacement2[i] = new PMXvVar();
				}
				replacement1[i].sequence = -1;
				replacement2[i].sequence = -1;
			}

			// STEP 3: Interchange
			for (int i = cuttingPoint1; i <= cuttingPoint2; i++) {
				offspring1Vector[i] = parent2Vector[i];
				offspring2Vector[i] = parent1Vector[i];

				replacement1[parent2Vector[i][1]].developer = parent1Vector[i][0];
				replacement1[parent2Vector[i][1]].sequence = parent1Vector[i][1];

				replacement2[parent1Vector[i][1]].developer = parent2Vector[i][0];
				replacement2[parent1Vector[i][1]].sequence = parent2Vector[i][1];
			} // for

			// STEP 4: Repair offsprings
			for (int i = 0; i < permutationLength; i++) {
				if ((i >= cuttingPoint1) && (i <= cuttingPoint2))
					continue;

				PMXvVar n1 = new PMXvVar();
				n1.developer = parent1Vector[i][0];
				n1.sequence = parent1Vector[i][1];

				PMXvVar m1 = replacement1[n1.sequence];

				PMXvVar n2 = new PMXvVar();
				n2.developer = parent2Vector[i][0];
				n2.sequence = parent2Vector[i][1];
				PMXvVar m2 = replacement2[n2.sequence];

				/// TODO: remove these count
				int count1 = 0;
				int count2 = 0;
				while (m1.sequence != -1) {
					count1++;
					n1 = m1;
					m1 = replacement1[m1.sequence];
					if (count1 == 100000) {
						System.out.println("here 1 ");
						break;
					}
				} // while
				while (m2.sequence != -1) {
					count2++;
					n2 = m2;
					m2 = replacement2[m2.sequence];
					if (count2 == 100000) {
						System.out.println("here 2");
					}
				} // while

				offspring1Vector[i][0] = n1.developer;
				offspring1Vector[i][1] = n1.sequence;

				offspring2Vector[i][0] = n2.developer;
				offspring2Vector[i][1] = n2.sequence;
			} // for
		} // if

		System.out.println();
		for (int i = 0; i < permutationLength; i++) {
			System.out.print(" {" + offspring1Vector[i][0] + "," + offspring1Vector[i][1] + "}, ");
		}
		System.out.println();
		for (int i = 0; i < permutationLength; i++) {
			System.out.print(" {" + offspring2Vector[i][0] + "," + offspring2Vector[i][1] + "}, ");

		}
		
		

	}
	
	 static public boolean isValidMixedPermutation (int[][] data, int len){
		  int [] ref = new int [len]   ;
		  for (int i=0; i<len; i++) {
			  ref[i] = -1;
		  }
		  for (int i=0; i<len; i++) {
		    if(ref[ data[i][1]] != -1 )	{
		    	return false;
		    }
		    ref[i] = data[i][1] ;
		  }
		  for (int i=0; i<len; i++) {
			  if(ref[i] == -1) {
				  return false;
			  }
		  }
		  return true;
	  }

}
